<?php
/**
 * Formulário do catálogo de entidades: ajudantes.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

# Formulário de edição de ajudantes.
$form = new easyForm( array(
	"submit"		=> gettext( "Save" ),
	"layout"		=> "form",
	"responsive"	=> true
) );

$form->addSection( array(
	"id"	=> "default",
	"cols"	=> 2,
	"title"	=> gettext( "Helper definitions" )
) );

$form->addSection( array(
	"id"	=> "helpText",
	"cols"	=> 1
) );

$form->addField( getObjectField( "catalogHelper", "helperId", array(
	"label"		=> gettext( "ID" ),
	"disabled"	=> true,
	"required"	=> false,
) ) );

$form->addField( getObjectField( "catalogHelper", "helperName", array(
	"label"			=> gettext( "Name" ),
	"legend"		=> gettext( "Enter the unique identifier for that record. Special characters other than \"_\" are not allowed." ),
	"break_line"	=> true
) ) );

$form->addField( getObjectField( "catalogHelper", "helperAllowTranslation", array(
	"label"		=> gettext( "Translate" ),
	"title"		=> gettext( "Enable automatic translations according to the system language." )
) ) );

$form->addField( getObjectField( "catalogHelper", "helperText", array(
	"label"		=> gettext( "Formatted help text" ),
	"legend"	=> gettext( "For this field, the use of Brazilian Portuguese texts for standardization is highly recommended." ),
	"section"	=> "helpText"
) ) );

# Tabela de listagem de registros na base.
$table =  new easyTable( array(
	"id"				=> "fusyHelpers",
	"title"				=> gettext( "Registered labels" ),
	"duplicable"		=> true,
	"modal_max_width"	=> 950,
	"data"				=> array(
		"object"		=> "catalogHelper",
		"key"			=> "helperId"
	),
	"columns"			=> array(
		array( "slug" => "helperId", "title" => gettext( "ID" ) ),
		array( "slug" => "helperName", "title" => gettext( "Name" ) ),
		array( "slug" => "helperAllowTranslation", "title" => gettext( "Do you have translations?" ), "type" => "bool" )
	)
) );

# Adicionar formulário à tabela para permitir ações com os registros.
$table->setForm( $form );
$table->getTable();