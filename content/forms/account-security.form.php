<?php
/**
 * Formulário de configurações de segurança do usuário.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$form = new easyForm( array(
	"cols"		=> 1,
	"submit"	=> gettext( "Save" )
) );

$form->addSection( array(
	"id"	=> "default",
	"title"	=> gettext( "Change username" )
) );

$form->addField( array(
	"id"		=> "username",
	"name"		=> "username",
	"type"		=> "username",
	"label"		=> gettext( "Username" ),
	"value"		=> getUserInfo( "username" ),
	"required"	=> true
) );

$form->addSection( array(
	"id"	=> "change-password",
	"title"	=> gettext( "Change password" )
) );

$form->addField( array(
	"id"		=> "password",
	"name"		=> "password",
	"type"		=> "password",
	"label"		=> gettext( "Current password" ),
	"section"	=> "change-password",
	"required"	=> true
) );

$form->addField( array(
	"id"		=> "newPassword",
	"name"		=> "newPassword",
	"type"		=> "password",
	"label"		=> gettext( "New password" ),
	"section"	=> "change-password",
	"required"	=> true
) );

$form->addField( array(
	"id"		=> "reTypeNewPassword",
	"name"		=> "reTypeNewPassword",
	"type"		=> "password",
	"clone"		=> "newPassword",
	"label"		=> gettext( "Retype new password" ),
	"section"	=> "change-password",
	"required"	=> true
) );

$form->getForm();