<?php
/**
 * Formulário para autenticação de usuários.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$form = new easyForm( array(
	"title"		=> gettext( "Access your account" ),
	"layout"	=> "form",
	"submit"	=> gettext( "Sign In" ),
	"append"	=> '<a href="' . addQueryArg( "user", "recovery", getPageURL() ) . '" class="forgot">' . gettext( "Forgot your password?" ) . '</a>',
	"before"	=> easyNotification::getNotification( "loginCallback", false ),
	"after"		=> ( ( getSetting( "allow_user_registration" ) === "yes" ) ? '<div class="signup">' . gettext( "Don't have an account yet?" ) . ' <a href="' . addQueryArg( "user", "signup", getPageURL() ) . '">' . gettext( "Click here to register" ) . '</a>.</div>' : "" )
) );

$form->addSection( array(
	"cols"	=> 1
) );

$form->addField( array(
	"id"		=> "login",
	"name"		=> "login",
	"label"		=> gettext( "Username" ),
	"required"	=> true
) );

$form->addField( array(
	"id"		=> "password",
	"name"		=> "password",
	"type"		=> "password",
	"label"		=> gettext( "Password" ),
	"required"	=> true,
	"validate"	=> false
) );

$form->addField( array(
	"id"		=> "remember",
	"name"		=> "remember",
	"type"		=> "checkbox",
	"class"		=> "remember-check",
	"title"		=> gettext( "Keep me connected" )
) );

$form->onSuccess( function( $response, $form ) {
	$session = new userSession( $response[ "login" ][ "db_value" ], $response[ "password" ][ "db_value" ], $response[ "remember" ][ "db_value" ] );
	$url = !is_null( getRequestParam( "redirect" ) ) ? urldecode( getRequestParam( "redirect" ) ) : addQueryArg( "user", "signin", getAppURL() );

	if( $error = $session->getError() )
		$form->setMessage( easyNotification::ERROR, $error );
	else
		$form->setSettings( array( "redirect" => $url ) );
});

$form->getForm();