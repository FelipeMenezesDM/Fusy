<?php
/**
 * Formulário para recuperação de senhas.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$form = new easyForm( array(
	"layout"	=> "form",
	"title"		=> gettext( "Password recovery" ),
	"subtitle"	=> gettext( "Enter your email to receive the password recovery link." ),
	"after"		=> '<a href="' . addQueryArg( "user", "signin", getPageURL() ) . '" class="cancel easyform-field type-button">' . gettext( "Cancel" ) . '</a>'
) );

$form->addField( array(
	"id"		=> "email",
	"name"		=> "email",
	"label"		=> gettext( "Enter your email" ),
	"required"	=> true
) );

$form->getForm();