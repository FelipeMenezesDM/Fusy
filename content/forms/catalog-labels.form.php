<?php
/**
 * Formulário do catálogo de entidades: rótulos.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

# Formulário de edição de rótulos.
$form = new easyForm( array(
	"cols"		=> 1,
	"submit"	=> gettext( "Save" )
) );

$form->addSection( array(
	"id"	=> "default",
	"title"	=> gettext( "Label definitions" )
) );

$form->addField( getObjectField( "catalogLabel", "labelId", array(
	"label"		=> gettext( "ID" ),
	"disabled"	=> true,
	"required"	=> false
) ) );

$form->addField( getObjectField( "catalogLabel", "labelName", array(
	"label"		=> gettext( "Name" ),
	"legend"	=> gettext( "Enter the unique identifier for that record. Special characters other than \"_\" are not allowed." )
) ) );

$form->addField( getObjectField( "catalogLabel", "labelDefText", array(
	"label"		=> gettext( "Default text" ),
	"legend"	=> gettext( "Standard text for the label, which will be used if there are no translations." )
) ) );

$form->addField( getObjectField( "catalogLabel", "labelHasTranslate", array(
	"label"		=> gettext( "Translate" ),
	"title"		=> gettext( "Use translations for the label" )
) ) );

$form->addField( getObjectField( "catalogLabel", "labelTranslations", array(
	"label"		=> gettext( "Translations" ),
	"keys"		=> array_column( getAvailableLanguages(), "value" )
) ) );

$form->addField( getObjectField( "catalogLabel", "labelContext", array(
	"label"		=> gettext( "Label context" ),
	"legend"	=> gettext( "Important to differentiate similar expressions, but which have different meanings. To fill this field, it is highly recommended to use Brazilian Portuguese text for standardization." )
) ) );

# Tabela de listagem de registros na base.
$table =  new easyTable( array(
	"id"			=> "fusyLabels",
	"title"			=> gettext( "Registered labels" ),
	"duplicable"	=> true,
	"data"			=> array(
		"object"	=> "catalogLabel",
		"key"		=> "labelId"
	),
	"columns"		=> array(
		array( "slug" => "labelId", "title" => gettext( "ID" ) ),
		array( "slug" => "labelName", "title" => gettext( "Name" ) ),
		array( "slug" => "labelDefText", "title" => gettext( "Default text" ) ),
		array( "slug" => "labelHasTranslate", "title" => gettext( "Do you have translations?" ), "type" => "bool" )
	)
) );

# Adicionar formulário à tabela para permitir ações com os registros.
$table->setForm( $form );
$table->getTable();