<?php
/**
 * Formulário de configurações de perfil do usuário.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$userMeta = getUserMeta( array( "color", "biography" ) );
$ppicture = getUserSavedImage();
$form = new easyForm( array(
	"cols"		=> 1,
	"autosave"	=> true,
	"submit"	=> gettext( "Save" )
) );

$form->addSection( array(
	"id"		=> "default",
	"title"		=> gettext( "Basic information" )
) );

$form->addField( getObjectField( "person", "personPPictureId", array(
	"label"		=> gettext( "Profile picture" ),
	"current"	=> array(
		"attach"	=> $ppicture[ "attach" ],
		"position"	=> $ppicture[ "position" ]
	),
	"attrs"		=> array( "data-url" => addQueryArgs( array( "handler" => "getUserProfileImages", "fullcontent" => true ), getAppURL() ) ),
	"default"	=> getLetterImage( getUserInfo( "fullname" ), $userMeta[ "color" ], 180 ),
	"autoSave"	=> "savePersonPicture"
) ) );

$form->addField( getObjectField( "person", "fullname", array(
	"label"		=> gettext( "Full name" ),
	"value"		=> getUserInfo( "fullname" )
) ) );

$form->addField( getObjectField( "person", "maidenName", array(
	"label"		=> gettext( "Maiden name" ),
	"value"		=> getUserInfo( "maidenName" )
) ) );

$form->addField( getObjectField( "person", "socialName", array(
	"label"		=> gettext( "Social name" ),
	"value"		=> getUserInfo( "socialName" )
) ) );

$form->addField( getObjectField( "person", "nickname", array(
	"label"		=> gettext( "Nickname" ),
	"value"		=> getUserInfo( "nickname" )
) ) );

$form->addField( getObjectField( "userMeta", "metaContent", array(
	"name"		=> "biography",
	"label"		=> gettext( "Biography" ),
	"format"	=> "textarea",
	"value"		=> $userMeta[ "biography" ],
	"autoSave"	=> "saveUserMeta"
) ) );

$form->addField( getObjectField( "person", "birthday", array(
	"label"		=> gettext( "Birthday" ),
	"value"		=> getUserInfo( "birthday" ),
	"required"	=> true
) ) );

$form->addField( getObjectField( "person", "breed", array(
	"value"		=> getUserInfo( "breed" ),
	"label"		=> gettext( "Breed" )
) ) );

$form->addField( getObjectField( "person", "gender", array(
	"label"		=> gettext( "Gender" ),
	"value"		=> getUserInfo( "gender" )
) ) );

$form->addField( getObjectField( "person", "maritalStatus", array(
	"value"		=> getUserInfo( "maritalstatus" ),
	"label"		=> gettext( "Marital status" )
) ) );

$form->addField( getObjectField( "person", "bloodType", array(
	"value"		=> getUserInfo( "bloodType" ),
	"label"		=> gettext( "Blood type" )
) ) );

$form->addSection( array(
	"id"		=> "social",
	"title"		=> gettext( "Social networks" )
) );

$form->addField( getObjectField( "userMeta", "metaContent", array(
	"name"		=> "site",
	"meta"		=> "social_networks",
	"value"		=> getUserMetaItem( "social_networks", "site" ),
	"label"		=> gettext( "Site" ),
	"section"	=> "social",
	"autoSave"	=> "saveUserMeta"
) ) );

$form->addField( getObjectField( "userMeta", "metaContent", array(
	"name"		=> "facebook",
	"meta"		=> "social_networks",
	"value"		=> getUserMetaItem( "social_networks", "facebook" ),
	"label"		=> gettext( "Facebook" ),
	"section"	=> "social",
	"autoSave"	=> "saveUserMeta"
) ) );

$form->addField( getObjectField( "userMeta", "metaContent", array(
	"name"		=> "twitter",
	"meta"		=> "social_networks",
	"value"		=> getUserMetaItem( "social_networks", "twitter" ),
	"label"		=> gettext( "Twitter" ),
	"section"	=> "social",
	"autoSave"	=> "saveUserMeta"
) ) );

$form->addField( getObjectField( "userMeta", "metaContent", array(
	"name"		=> "instagram",
	"meta"		=> "social_networks",
	"value"		=> getUserMetaItem( "social_networks", "instagram" ),
	"label"		=> gettext( "Instagram" ),
	"section"	=> "social",
	"autoSave"	=> "saveUserMeta"
) ) );

$form->addField( getObjectField( "userMeta", "metaContent", array(
	"name"		=> "youtube",
	"meta"		=> "social_networks",
	"value"		=> getUserMetaItem( "social_networks", "youtube" ),
	"label"		=> gettext( "Youtube" ),
	"section"	=> "social",
	"autoSave"	=> "saveUserMeta"
) ) );

$form->addField( getObjectField( "userMeta", "metaContent", array(
	"name"		=> "linkedin",
	"meta"		=> "social_networks",
	"value"		=> getUserMetaItem( "social_networks", "linkedin" ),
	"label"		=> gettext( "LinkedIn" ),
	"section"	=> "social",
	"autoSave"	=> "saveUserMeta"
) ) );

$form->addField( getObjectField( "userMeta", "metaContent", array(
	"name"		=> "skype",
	"meta"		=> "social_networks",
	"value"		=> getUserMetaItem( "social_networks", "skype" ),
	"label"		=> gettext( "Skype" ),
	"section"	=> "social",
	"autoSave"	=> "saveUserMeta"
) ) );

$form->addField( getObjectField( "userMeta", "metaContent", array(
	"name"		=> "github",
	"meta"		=> "social_networks",
	"value"		=> getUserMetaItem( "social_networks", "github" ),
	"label"		=> gettext( "GitHub" ),
	"section"	=> "social",
	"autoSave"	=> "saveUserMeta"
) ) );

$form->addSection( array(
	"id"		=> "location",
	"title"		=> gettext( "Language and region" )
) );

$form->addField( getObjectField( "userMeta", "metaContent", array(
	"name"		=> "language",
	"label"		=> gettext( "Language" ),
	"section"	=> "location",
	"type"		=> "select",
	"options"	=> getAvailableLanguages(),
	"required"	=> true,
	"value"		=> getSystemLanguage(),
	"autoSave"	=> "saveUserMeta",
	"onSuccess"	=> function() {
		global $field;
		setSystemLanguage( $field[ "value" ] );
	}
) ) );

$form->addField( getObjectField( "userMeta", "metaContent", array(
	"name"		=> "timezone",
	"label"		=> gettext( "Timezone" ),
	"section"	=> "location",
	"type"		=> "timezone",
	"required"	=> true,
	"value"		=> getSystemTimezone(),
	"autoSave"	=> "saveUserMeta",
	"onSuccess"	=> function() {
		global $field;
		setSystemLanguage( $field[ "value" ] );
	}
) ) );

$form->setRegistredItemID( getUserInfo( "personId" ) );
$form->autoSave( "saveProperty" );
$form->getForm();