<?php
/**
 * Formulário para autenticação de usuários.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$form = new easyForm( array(
	"layout"	=> "form",
	"title"		=> gettext( "Register" ),
	"subtitle"	=> gettext( "Make your registration by filling out the information below." ),
	"submit"	=> gettext( "Sign Up" ),
	"after"		=> '<div class="signin">' . gettext( "Already have an account?" ) . ' <a href="' . addQueryArg( "user", "signin", getPageURL() ) . '">' . gettext( "Click here to login" ) . '</a>.</div>'
) );

$form->addSection( array(
	"id"	=> "default",
	"cols"	=> 2
) );

$form->addSection( array(
	"id"	=> "default2",
	"cols"	=> 1
) );

$form->addField( getObjectField( "person", "fullname", array(
	"name"		=> "name",
	"label"		=> gettext( "Name" ),
	"section"	=> "default",
	"max"		=> 25
) ) );

$form->addField( getObjectField( "person", "fullname", array(
	"name"		=> "surname",
	"label"		=> gettext( "Surname" ),
	"section"	=> "default",
	"max"		=> getObjectFieldParam( "person", "fullname", "length" ) - 26
) ) );

$form->addField( getObjectField( "person", "socialName", array(
	"label"		=> gettext( "Social name" ),
	"section"	=> "default2"
) ) );

$form->addField( getObjectField( "person", "nickname", array(
	"label"		=> gettext( "Nickname" ),
	"section"	=> "default2"
) ) );

$form->addField( getObjectField( "email", "emailAddress", array(
	"label"		=> gettext( "E-mail" ),
	"section"	=> "default2"
) ) );

$form->addField( getObjectField( "user", "username", array(
	"label"		=> gettext( "Username" ),
	"section"	=> "default2"
) ) );

$form->addField( getObjectField( "person", "birthday", array(
	"label"		=> gettext( "Birthday" ),
	"section"	=> "default2"
) ) );

$form->addField( getObjectField( "person", "gender", array(
	"label"		=> gettext( "Gender" ),
	"class"		=> "select-gender",
	"section"	=> "default2"
) ) );

$form->addField( getObjectField( "user", "password", array(
	"label"		=> gettext( "Password" ),
	"section"	=> "default2"
) ) );

$form->addField( getObjectField( "user", "password", array(
	"name"		=> "confirmPassword",
	"clone"		=> "password",
	"label"		=> gettext( "Confirm password" ),
	"section"	=> "default2"
) ) );

$form->addField( array(
	"name"		=> "remember",
	"type"		=> "checkbox",
	"class"		=> "remember-check",
	"title"		=> gettext( "Keep me connected" ),
	"section"	=> "default2"
) );

$form->onSuccess( function( $resp, $form ) {
	$personHash = uuid();
	$person = new Person();
	$person->setFullname( $resp[ "name" ][ "db_value" ] . " " . $resp[ "surname" ][ "db_value" ] );
	$person->setSocialName( $resp[ "socialname" ][ "db_value" ] );
	$person->setNickname( $resp[ "nickname" ][ "db_value" ] );
	$person->setPersonHash( $personHash );
	$person->setBirthday( $resp[ "birthday" ][ "db_value" ] );
	$person->setGender( $resp[ "gender" ][ "db_value" ] );
	$person->setType( "F" );

	# Cadastrar pessoa no sistema.
	if( !$person->post() ) {
		$form->setMessage( easyNotification::ERROR, gettext( "It was not possible to register the user in the system. Please check the data provided and try again." ) );
		return;
	}

	# Obter pessoa cadastrada a partir de seu hash único.
	$person->getByHash( $personHash );

	$email = new Email();
	$email->setEmailAddress( $resp[ "emailaddress" ][ "db_value" ] );
	$email->setEmailIsPrimary( 1 );
	$email->setEmailPersonId( $person->getPersonId() );

	# Cadastrar endereço de e-mail no sistema.
	if( !$email->post() ) {
		$person->delete();
		$form->setMessage( easyNotification::ERROR, gettext( "It was not possible to register the user in the system. Please check the data provided and try again." ) );
		return;
	}

	#  Obter endereço de e-mail.
	$email->getByEmailAddress( $resp[ "emailaddress" ][ "db_value" ] );

	$user = new User();
	$user->setUsername( $resp[ "username" ][ "db_value" ] );
	$user->setPassword( $resp[ "password" ][ "db_value" ] );
	$user->setUserPersonId( $person->getPersonId() );
	
	# Castrar usuário no sistema.
	if( !$user->post() ) {
		$email->delete();
		$person->delete();
		$form->setMessage( easyNotification::ERROR, gettext( "It was not possible to register the user in the system. Please check the data provided and try again." ) );
		return;
	}

	$user->getByUsername( $resp[ "username" ][ "db_value" ] );

	# Definir cor padrão do usuário.
	putUserMeta( "color", letterVector::getRandColor(), "TEXT", $user->getUserID() );

	# Criar sessão do usuário.
	$userSession = new userSession( $resp[ "emailaddress" ][ "db_value" ], $resp[ "password" ][ "value" ], $resp[ "remember" ][ "db_value" ] );
	$url = !is_null( getRequestParam( "redirect" ) ) ? urldecode( getRequestParam( "redirect" ) ) : addQueryArg( "user", "signin", getAppURL() );

	# Redirecionamento de página.
	$form->setSettings( array( "redirect" => $url ) );
});

$form->getForm();