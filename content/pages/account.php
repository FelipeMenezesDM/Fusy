<?php
/**
 * Página principal de configuração de conta do usuário.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$tabs = new goldTabs( "section" );

$tabs->addTab( array(
	"title"		=> gettext( "Profile" ),
	"legend"	=> gettext( "Edit your profile" ),
	"handler"	=> "profile",
	"default"	=> true,
	"form"		=> "account-profile"
) );

$tabs->addTab( array(
	"title"		=> gettext( "User contacts" ),
	"legend"	=> gettext( "Change your user contacts" ),
	"handler"	=> "user-contacts",
	"form"		=> "account-user-contacts"
) );

$tabs->addTab( array(
	"title"		=> gettext( "Security and login" ),
	"legend"	=> gettext( "Configure your security" ),
	"handler"	=> "account-security",
	"form"		=> "account-security"
) );

$tabs->addTab( array(
	"title"		=> gettext( "Privacy" ),
	"legend"	=> gettext( "Manage your privacy" ),
	"handler"	=> "privacy",
	"form"		=> "account-privacy"
) );

$tabs->addTab( array(
	"title"		=> gettext( "Notifications" ),
	"legend"	=> gettext( "Customize your notifications" ),
	"handler"	=> "notification",
	"form"		=> "account-notification"
) );

$tabs->getTabs();