<?php
/**
 * Página de erro 404.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

getHeader();

?>

<div class="error404-content-wrapper">
	<div class="error404-body">
		<h1>404</h1>
		<h3><?php echo gettext( "Page not found" ); ?></h3>
		<p><?php echo gettext( "Sorry, but the link you followed may be broken or removed." ); ?></p>
		<a href="<?php echo getAppURL(); ?>" class="easyform-field type-button primary"><i class="fas fa-home"></i> <?php echo gettext( "Return to home page" ); ?></a>
	</div>
</div>

<?php getFooter(); ?>