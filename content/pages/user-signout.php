<?php
/**
 * Página para encerrar sessão do usuário.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$sessions = getUserSessions();

foreach( $sessions as $sessionKey => $userSession ) {
	if( getRequestParam( "all" ) === "true" || $userSession[ "sessionid" ] === getUserInfo( "sessionId" ) ) {
		$session = new Session();
		$session->setSessionID( $userSession[ "sessionid" ] );
		$session->setSessionStatus( 0 );
		$session->setSessionLastAccess( date( "Y-m-d H:i:s" ) );
		$session->put();
	}
}

# URL de redirecionamento.
$url = addQueryArg( "user", "signin", getAppURL() );

if( !is_null( getRequestParam( "redirect" ) ) )
	$url = addQueryArg( "redirect", urlencode( getRequestParam( "redirect" ) ), $url );

redirectWithoutCache( $url );