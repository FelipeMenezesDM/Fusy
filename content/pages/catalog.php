<?php
/**
 * Página principal do catálogo de entidades.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$linkCards = new LinkCard( "entity" );

$linkCards->addCard( array(
	"handler"	=> "tables",
	"form"		=> "catalog-tables",
	"title"		=> gettext( "Tables" ),
	"icon"		=> "table"
) );

$linkCards->addCard( array(
	"handler"	=> "attributes",
	"form"		=> "catalog-attributes",
	"title"		=> gettext( "Attributes" ),
	"icon"		=> "magnet"
) );

$linkCards->addCard( array(
	"handler"	=> "parameters",
	"form"		=> "catalog-parameters",
	"title"		=> gettext( "Parameters" ),
	"icon"		=> "cubes"
) );

$linkCards->addCard( array(
	"handler"	=> "views",
	"form"		=> "catalog-views",
	"title"		=> gettext( "Views" ),
	"icon"		=> "list-alt"
) );

$linkCards->addCard( array(
	"handler"	=> "forms",
	"form"		=> "catalog-forms",
	"title"		=> gettext( "Forms" ),
	"icon"		=> "clipboard-list"
) );

$linkCards->addCard( array(
	"handler"	=> "pages",
	"form"		=> "catalog-pages",
	"title"		=> gettext( "Pages" ),
	"icon"		=> "newspaper"
) );

$linkCards->addCard( array(
	"handler"	=> "locatores",
	"form"		=> "catalog-locatores",
	"title"		=> gettext( "Locators" ),
	"icon"		=> "search"
) );

$linkCards->addCard( array(
	"handler"	=> "labels",
	"form"		=> "catalog-labels",
	"title"		=> gettext( "Labels" ),
	"icon"		=> "language"
) );

$linkCards->addCard( array(
	"handler"	=> "helpers",
	"form"		=> "catalog-helpers",
	"title"		=> gettext( "Helpers" ),
	"icon"		=> "hands-helping"
) );

$linkCards->getCards();