<?php
/**
 * Página para alternação de sessões ativar do usuário.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

# URL de redirecionamento.
$url = !is_null( getRequestParam( "redirect" ) ) ? urldecode( getRequestParam( "redirect" ) ) : addQueryArg( "user", "signin", getAppURL() );
$sessions = getUserSessions();
$id = getRequestParam( "id" );
$key = "";

foreach( $sessions as $sessionKey => $session ) {
	if( $session[ "sessionid" ] === $id )
		$key = $sessionKey;
}

# Verificar se é permitido criar várias sessões do usuário no sistema.
if( getSetting( "allow_user_multiple_session" ) !== "yes" || empty( $sessions ) || empty( $id ) || empty( $key ) )
	redirectWithoutCache( $url );

# Alterar sessões.
userSession::removeCookie( $key );
userSession::setCookie( $key, $id );

redirectWithoutCache( $url );