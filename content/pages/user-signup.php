<?php
/**
 * Página de cadastro do usuário.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

# URL de redirecionamento.
$url = !is_null( getRequestParam( "redirect" ) ) ? urldecode( getRequestParam( "redirect" ) ) : addQueryArg( "user", "signin", getAppURL() );

# Verificar se cadastro externo de usuários é permitido.
if( getSetting( "allow_user_registration" ) !== "yes" )
	redirectWithoutCache( $url );

getHeader();

?>

<div class="user-content-wrapper">
	<div class="user-form-wrapper">
		<div class="user-form-body">
			<?php getForm( "signup" ); ?>
		</div>
	</div>
</div>

<?php getFooter(); ?>