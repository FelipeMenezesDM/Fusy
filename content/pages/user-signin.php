<?php
/**
 * Página de autenticação do usuário.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

# Redirecionar, caso a sessão de usuário já exista.
checkAuthAccess( false );
getHeader();

?>

<div class="user-content-wrapper">
	<div class="user-form-wrapper">
		<div class="user-form-body">
			<?php getForm( "signin" ); ?>
		</div>
	</div>
</div>

<?php getFooter(); ?>