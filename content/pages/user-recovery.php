<?php
/**
 * Página de recuperação de senha.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

getHeader();

?>

<div class="user-content-wrapper">
	<div class="user-form-wrapper">
		<div class="user-form-body">
			<?php getForm( "recovery" ); ?>
		</div>
	</div>
</div>

<?php getFooter(); ?>