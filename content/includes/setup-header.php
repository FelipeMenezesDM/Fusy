<?php
/**
 * Cabeçalho padrão para as páginas do painel de administração do sistema.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$url = getAppURL();

?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="system-name" content="<?php echo getSetting( "system_name" ); ?>" />
	<title><?php echo getPageTitle(); ?></title>
	<?php getStyles( "setup" ); # Carregamento de estilos ?>
</head>

<body>
	<div class="wrapper">
		<div class="setup-header ef-table-height-line">
			<div class="user-main-options to-right">
				<div class="user-special-options">
					<div class="user-opt"><a href="<?php echo addQueryArgs( array( "setup" => "favorites" ), $url ); ?>"><i class="fas fa-star"></i></a></div>
					<div class="user-opt drop-down-container">
						<a href="<?php echo getMenuURL( "setup", "messages" ); ?>" data-tab-ignore="true" class="drop-down-handler"><i class="fas fa-comments"></i></a>
						<div class="floating-box drop-down-menu">
						</div>
					</div>
					<div class="user-opt drop-down-container">
						<a href="<?php echo getMenuURL( "setup", "notifications" ); ?>" data-tab-ignore="true" class="drop-down-handler"><i class="fas fa-bell"></i></a>
						<div class="floating-box drop-down-menu">
						</div>
					</div>
					<div class="user-opt"><a href="<?php echo addQueryArgs( array( "setup" => "help" ), $url ); ?>"><i class="fas fa-question-circle"></i></a></div>
				</div><!-- .user-special-options -->
				<div class="vr"></div>
				<div class="user-options drop-down-container" tabindex="0">
					<div class="user-info drop-down-handler">
						<div class="ui-picture"><?php echo getUserImage( 50 ); ?></div>
						<div class="ui-name">
							<h4><?php echo getUserDisplayName(); ?></h4>
							<h5><?php echo getUserInfo( "username" ); ?></h5>
						</div>
					</div><!-- .user-info -->
					<div class="user-floating-box floating-box user-menu drop-down-menu">
						<ul>
						<?php
							if( getSetting( "allow_user_multiple_session" ) === "yes" ) {
								# Listar sessões ativas.
								foreach( getUserSessions() as $session ) {
									if( $session[ "userid" ] === getUserID() )
										continue;

									$name = $session[ "fullname" ];
									$url = addQueryArgs( array( "user" => "change-session", "id" => $session[ "sessionid" ], "redirect" => getPageURL() ), $url );

									if( !empty( $session[ "nickname" ] ) )
										$name = $session[ "nickname" ];

									echo '<li><a href="' . $url . '" data-tab-ignore="true"><div class="icon">' . getUserImage( 130, true, $session[ "userid" ] ) . '</div>' . $name . '</a></li>';
								}

								echo '<li><a href="' . addQueryArgs( array( "user" => "add-session", "redirect" => getPageURL() ), $url ) . '" data-tab-ignore="true"><div class="icon"><i class="fas fa-user-plus"></i></div>' . gettext( "Add another account" ) .'</a></li><hr>';
							}
						?>
							<li><a href="<?php echo getMenuURL( "setup", "profile" ); ?>"><div class="icon"><i class="fas fa-user"></i></div><?php echo gettext( "Profile" ); ?></a></li>
							<li><a href="<?php echo getMenuURL( "setup", "user-contacts" ); ?>"><div class="icon"><i class="fas fa-envelope"></i></div><?php echo gettext( "User contacts" ); ?></a></li>
							<li><a href="<?php echo getMenuURL( "setup", "account-security" ); ?>"><div class="icon"><i class="fas fa-shield-alt"></i></div><?php echo gettext( "Security and login" ); ?></a></li>
							<li><a href="<?php echo getMenuURL( "setup", "privacy" ); ?>"><div class="icon"><i class="fas fa-lock"></i></div><?php echo gettext( "Privacy" ); ?></a></li>
							<li><a href="<?php echo getMenuURL( "setup", "notification" ); ?>"><div class="icon"><i class="fas fa-bell"></i></div><?php echo gettext( "Notifications" ); ?></a></li>
							<hr>
							<li><a href="<?php echo getMenuURL( "setup-footer", "help" ); ?>"><div class="icon"><i class="fas fa-question"></i></div><?php echo gettext( "Help" ); ?></a></li>
							<li><a href="<?php echo getMenuURL( "setup-footer", "report" ); ?>"><div class="icon"><i class="fas fa-comment-dots"></i></div><?php echo gettext( "Report problem" ); ?></a></li>
							<hr>
							<li><a href="<?php echo addQueryArgs( array( "user" => "signout", "redirect" => getPageURL() ), $url ); ?>" data-tab-ignore="true"><div class="icon"><i class="fas fa-sign-out-alt"></i></div><?php echo gettext( "Logout" ); ?></a></li>
							<li><a href="<?php echo addQueryArgs( array( "user" => "signout", "all" => "true", "redirect" => getPageURL() ), $url ); ?>" data-tab-ignore="true"><div class="icon"><i class="fas fa-power-off"></i></div><?php echo gettext( "End all sessions" ); ?></a></li>
						</ul>
					</div><!-- .user-menu -->
				</div><!-- .user-options -->
			</div><!-- .user-main-options -->
			<div class="clearfix"></div>
		</div><!-- .setup-header -->

		<div class="setup-menu">
			<div class="setup-main-menu"><?php getMenu( "setup" ); ?></div>
			<div class="setup-basic-menu"><?php getMenu( "setup-footer" ); ?></div>
		</div>