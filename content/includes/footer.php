<?php
/**
 * Rodapé padrão para as páginas externas do sistema.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */
?>
	</div><!-- .wrapper -->
	<?php getScripts( "easyform" ); # Carregamento de scripts ?>
</body>