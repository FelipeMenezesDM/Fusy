<?php
/**
 * Cabeçalho padrão para as páginas externas do sistema.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */
?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title><?php echo getPageTitle(); ?></title>
	<?php getStyles( "general" ); # Carregamento de estilos ?>
</head>

<body>
	<div class="wrapper">