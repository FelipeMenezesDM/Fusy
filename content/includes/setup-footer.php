<?php
/**
 * Rodapé padrão para as páginas do painel de administração do sistema.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */
?>

		<div class="setup-footer ef-table-height-line">
			<div class="setup-footer-wrapper">
				<div class="copy"><?php printf( gettext( "%s %s by %s — All rights reserved." ), "&copy;", date( "Y" ), getSetting( "system_name" ) ); ?></div>
				<div class="version">
					<span><b><?php echo getVersion(); ?></b></span>
					<span><?php echo date( "d/m/Y" ); ?></span>
					<span><?php echo date( "H:i" ); ?></span>
					<span><?php echo date( "e (P)" ); ?></span>
				</div>
			</div>
		</div>
	</div><!-- .wrapper -->
	<?php getScripts( "setup" ); # Carregamento de scripts ?>
</body>