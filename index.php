<?php
/**
 * Página de inicialização do projeto.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

# Iniciar sessão.
session_start();

# Constantes globais do framework.
define( "PATH", __DIR__ );
define( "SEP", DIRECTORY_SEPARATOR );

# Incluir arquivos de funções do framework.
require_once( PATH . SEP . "setup" . SEP . "functions.php" );
getSetupPage( "functions-objects" );

# Incluir autoload das dependências do framework.
if( !_require( "./libs/autoload.php" ) ) {
	echo "The file for loading dependencies could not be found. Please contact your system administrator.";
	exit;
}

# Incluir arquivo de inicialização.
getSetupPage( "init" );

# Incluir autoload de classes do framework.
getSetupPage( "autoload" );

# Definindo URL padrão de ações de tabelas EasyTable.
easyTable::setDefaultActionURL( mainEntity::getRequestURL() );

# Definir estilos.
setStyle( "font-awesome", getAppURL() . "libs/felipemenezesdm/easyform/assets/css/font-awesome/css/solid.min.css" );
setStyle( "font-awesome", getAppURL() . "libs/felipemenezesdm/easyform/assets/css/font-awesome/css/fontawesome.min.css" );
setStyle( "google-fontes", "https://fonts.googleapis.com/css2?family=Commissioner:wght@400;600&display=swap" );
setStyle( "easyform", getAppURL() . "libs/felipemenezesdm/easyform/assets/css/easyform-style.css", array( "font-awesome", "google-fontes" ) );
setStyle( "easytable", getAppURL() . "libs/felipemenezesdm/easyform/assets/css/easyform-tables-style.css", array( "easyform" ) );
setStyle( "general", getStylesURI( "style" ), array( "easyform" ), "1.0.0.0" );
setStyle( "setup", getStylesURI( "style-setup" ), array( "easytable" ), "1.0.0.0" );
setStyle( "installation", getStylesURI( "style-installation" ), array( "easyform" ), "1.0.0.0" );

# Definir scripts.
setScript( "jquery", getAppURL() . "libs/felipemenezesdm/easyform/assets/js/jquery/jquery.js", array(), "3.3.1" );
setScript( "translations", getScriptsURI( "translations", "php" ), array( "jquery" ), "1.0.0.0" );
setScript( "easyform", getAppURL() . "libs/felipemenezesdm/easyform/assets/js/easyform-includes.js", array( "translations" ), "1.0.0.0" );
setScript( "setup", getScriptsURI( "setting-setup" ), array( "easyform" ), "1.0.0.0" );

# Iniciar módulo de instalação, caso necessário.
if( !defined( "SYSTEM_STATUS" ) ) {
	getSetupPage( "installation/index" );
	exit;
}

/**
 * Variáveis globais do sistema.
 * $settings > Obter configurações do sistema.
 * $pageTitle > Título principal da página.
 */
global $settings, $pageTitle;
$settings = getSettings();
setPageTitle( getSetting( "system_name" ) );

# Obter sessão válida do usuário.
global $userSessions, $currentUser;
$userSessions = userSession::getSessions();

# Definir diretório padrão de uploads.
$uploadsFolder = ( !is_null( $currentUser ) ? getUserInfo( "username" ) : getSetting( "system_name" ) );
easyUpload::setGlobalUploadDirectory( ini_get( "upload_tmp_dir" ) . "/fusy-uploads/" . $uploadsFolder . "/" . date( "Y-m-d/" ) );

# Inicializar menus.
if( isSetup() ) {
	initSetupNavigation();

	global $setupMenu, $currentSetupMenuItem;
	$setupMenu = getMenu( "setup", false );

	# Obter item selecionado a partir do menu principal ou do básico.
	if( !( $currentSetupMenuItem = $setupMenu->getCurrentItem() ) ) {
		$setupMenu = getMenu( "setup-footer", false );
		$currentSetupMenuItem = $setupMenu->getCurrentItem();
	}
}else{
	initNavigation();
}

# Incluir todas as páginas do sistema, conforme requisição.
pages();