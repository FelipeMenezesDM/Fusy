<?php
/**
 * Objetos de traduções para JavaScript.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$jsTranslations = array(
	"New tab" => gettext( "New tab" ),
	"We were unable to finish loading the page. Please check your internet connection." => gettext( "We were unable to finish loading the page. Please check your internet connection." ),
	"Do you really want to remove this page from your favorites?" => gettext( "Do you really want to remove this page from your favorites?" ),
	"The request could not be completed. Please try again." => gettext( "The request could not be completed. Please try again." ),
	"This page has been successfully removed from favorites." => gettext( "This page has been successfully removed from favorites." ),
	"Save new bookmark" => gettext( "Save new bookmark" ),
	"The request could not be completed. Please try again." => gettext( "The request could not be completed. Please try again." ),
	"Select the folder:" => gettext( "Select the folder:" ),
	"Create new folder" => gettext( "Create new folder" ),
	"New folder name:" => gettext( "New folder name:" )
);

# Imprimir mensagens traduzidas.
echo "var translations = $.extend( true, translations || {}, " . json_encode( $jsTranslations ) . ");";