/**
 * Configurações gerais do painel de administração.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

jQuery( document ).ready( function( $ ) { "use strict";
	/**
	 * Simular evento focus.
	 * @param object e Objeto do evento.
	 */
	$( "body" ).on( "mousedown", function( e ) {
		if( $( e.target ).closest( ".drop-down-container" ).length === 0 )
			$( ".drop-down-handler.actived" ).trigger( "mousedown" );
	}).on( "mouseup", ".drop-down-container.drop-down-autoclose .drop-down-menu", function() {
		$(this).closest( ".drop-down-autoclose" ).find( ".drop-down-handler.actived" ).trigger( "mousedown" );
	});

	/**
	 * Efeito de exibição/ocultação de submenus.
	 */
	$( "body" ).on( "mousedown", ".drop-down-handler", function() {
		$( ".drop-down-handler.actived" ).not( $(this) ).trigger( "mousedown" );

		if( $(this).is( ".actived" ) )
			$(this).removeClass( "actived" ).closest( ".drop-down-container" ).removeClass( "show" ).find( ".drop-down-menu" ).stop().fadeOut( 100 );
		else
			$(this).addClass( "actived" ).closest( ".drop-down-container" ).addClass( "show" ).find( ".drop-down-menu" ).stop().fadeIn( 200 );
	}).on( "click", ".drop-down-handler", function() {
		return false;
	});

	/**
	 * Efeito de exibição/ocultação de submenus no menu lateral.
	 */
	$( "body" ).on( "click", ".setup-menu li.has-submenu > a", function() {
		var item = $(this).closest( "li.has-submenu" ).toggleClass( "selected" );
		item.children( ".submenu" ).toggle( !item.is( ".selected" ) ).stop().slideToggle( 200 );
		return false;
	});

	/**
	 * Lista de abas ativas do sistema.
	 */
	var navTabs = {};

	/**
	 * Ler/atualizar lista de abas abertas.
	 */
	$( "body" ).on( "updateTabsList initTabs", function( e ) {
		var itens = $(this).find( ".mini-tab" ),
			container = itens.closest( ".mini-tabs-itens" ),
			containerWidth = 0;
			navTabs = {};

		if( itens.length === 1 )
			itens.removeClass( "no-cached" );

		// Limpando navegação básica de abas.
		$( ".mini-tabs-simple-nav ul" ).html( "" );

		itens.each( function() {
			var tab = $(this),
				id = tab.data( "tab-id" ),
				page = $( ".mini-tabs-pages .mini-tab-page" ).getByData( "tab-id", id );

			// Adicionando aba à lista de abast abertas.
			navTabs[ ( id ) ] = {
				"id": id,
				"tab": tab,
				"title": tab.data( "tab-title" ) || getSystemName(),
				"url": tab.data( "tab-url" ),
				"page": page,
				"request": tab.data( "ajax-request" ) || null
			};

			// Definir visibilidade do botão de fechamento da aba do item padrão.
			if( tab.is( ".is-default" ) )
				tab.toggleClass( "fixed-tab", !( itens.length > 1 ) );

			// Adicionar itens de abas no menu de navegação básica.
			$( '<li><a>' + tab.attr( "title" ) + '</a></li>' ).appendTo( ".mini-tabs-simple-nav ul" ).find( "a" ).data( "tab-id", id );

			// Atualizar largura do container.
			containerWidth += tab.outerWidth() + 10;
		});

		container.width( containerWidth );

		// Ativar reposicionamento de abas.
		if( e.type !== "initTabs" )
			container.sortable({ axis: "x" });

		// Atualizar navegação de abas.
		$( window ).trigger( "updateTabsBox" );
	}).trigger( "initTabs" );

	/**
	 * Exibir/ocutar navegação de abas de acordo com o tamanho da tela.
	 */
	$( window ).on( "resize load updateTabsBox", function() {
		var container = $( ".mini-tabs-container" ),
			first = container.find( ".mini-tab:first" ),
			last = container.find( ".mini-tab:last" );

		// Exibir opções de navegação de abas.
		$( ".mini-tabs-box" ).toggleClass( "overflowing", ( $( ".mini-tabs-itens" ).outerWidth() - 10 > $( ".mini-tabs-box" ).outerWidth() ) );

		// Ativar desativar botões de navegação, de acordo com a posição das abas.
		$( ".mini-tabs-nav-act.prev" ).toggleClass( "disabled", ( first.offset().left === container.offset().left ) );
		$( ".mini-tabs-nav-act.next" ).toggleClass( "disabled", ( last.offset().left + last.outerWidth() < container.offset().left + container.outerWidth() ) );
	});

	/**
	 * Atualizar opções de navegação durante o scroll do container de abas.
	 */
	$( ".mini-tabs-container" ).on( "scroll", function() {
		$( window ).trigger( "updateTabsBox" );
	});

	/**
	 * Evento de click para acionar aba correspondente ao link da navegação básica.
	 */
	$( "body" ).on( "click", ".mini-tabs-simple-nav a", function() {
		navTabs[ ( $(this).data( "tab-id" ) ) ].tab.trigger( "click" );
	});

	/**
	 * Eventos de navegação para abas.
	 */
	$( "body" ).on( "click", ".mini-tabs-nav-act.next, .mini-tabs-nav-act.prev", function() {
		if( $(this).is( ".disabled" ) )
			return false;

		if( $(this).is( ".prev" ) ) {
			$( $( ".mini-tabs-itens .mini-tab" ).get().reverse() ).each( function() {
				var handler = $(this),
					container = handler.closest( ".mini-tabs-container" );

				if( handler.offset().left < container.offset().left ) {
					container.stop().animate({ scrollLeft: handler.position().left }, 200 );
					return false;
				}
			});
		}else if( $(this).is( ".next" ) ) {
			$( ".mini-tabs-itens .mini-tab" ).each( function() {
				var handler = $(this),
					container = handler.closest( ".mini-tabs-container" );

				if( handler.offset().left + handler.outerWidth() > container.offset().left + container.outerWidth() ) {
					container.stop().animate({ scrollLeft: ( handler.position().left + handler.outerWidth() ) - ( container.outerWidth() - 10 ) }, 200 );
					return false;
				}
			});
		}
	});

	/**
	 * Nevagação a partir do histórico de abas.
	 */
	$( window ).on( "popstate", function( e ) {
		if( !e.originalEvent.state )
			return;

		if( navTabs[ ( e.originalEvent.state.tabId ) ] )
			navTabs[ ( e.originalEvent.state.tabId ) ].tab.trigger( "showTab" );
	});

	/**
	 * Eventos de click para mini abas.
	 */
	$( "body" ).on( "click showTab", ".mini-tabs-itens .mini-tab", function( e ) {
		var tabHandler = navTabs[ ( $(this).data( "tab-id" ) ) ],
			tabId = tabHandler.id,
			itemId = $(this).data( "item-id" ) || tabId;

		// Selecionar aba.
		$( ".mini-tabs-itens" ).find( ".mini-tab" ).removeClass( "selected" ).filter( tabHandler.tab ).addClass( "selected" );
		$( ".mini-tabs-pages" ).find( ".mini-tab-page" ).hide().filter( tabHandler.page ).show();

		// Marcar itens do menu principal de acordo com a aba selecionada.
		$( ".setup-main-menu ul.menu-setup" ).find( ".hightlight" ).removeClass( "hightlight ajax-nav" ).end()
		.find( "li.item-" + itemId ).addClass( "hightlight ajax-nav" ).parents( ".has-submenu" ).addClass( "hightlight ajax-nav" );

		// Marcar itens do menu de navegação simples para abas.
		$( ".mini-tabs-simple-nav" ).find( "li" ).removeClass( "current" ).find( "a" ).getByData( "tab-id", tabId ).parent().addClass( "current" );

		// Atualizar título da página durante navegação.
		document.title = tabHandler.title;

		// Atualizar histórico de abas abertas.
		if( e.type !== "showTab" && history.pushState !== undefined && ( !history.state || document.URL !== tabHandler.url ) )
			history.pushState({ tabId }, tabHandler.title, tabHandler.url );

		var container = $(this).closest( ".mini-tabs-container" ),
			handlerOffset = $(this).offset().left,
			handlerOffsetFull = handlerOffset + $(this).outerWidth(),
			handlerPosition = $(this).position().left,
			handlerPositionFull = handlerPosition + $(this).outerWidth(),
			containerOffset = container.offset().left,
			containerOffsetFull = containerOffset + container.outerWidth();

		// Reposicionamento de abas.
		if( handlerOffset < containerOffset )
			container.stop().animate({ scrollLeft: handlerPosition }, 300 );
		else if( handlerOffsetFull > containerOffsetFull )
			container.stop().animate({ scrollLeft: handlerPositionFull - ( container.outerWidth() - 10 ) }, 300 );
	}).on( "click", ".mini-tabs-itens .mini-tab .close-act", function() {
		// Carregar página padrão quando a última aba for fechada.
		if( Object.keys( navTabs ).length === 1 ) {
			$( ".setup-main-menu li.is-default > a" ).trigger( "loadDefaultPage" );
			return false;
		}

		var tabHandler = navTabs[ ( $(this).closest( ".mini-tab" ).data( "tab-id" ) ) ];

		// Cancelar requisição ajax.
		if( tabHandler.request !== null ) {
			navTabs[ ( id ) ].request.abort();
			navTabs[ ( id ) ].request = null;
		}

		// Selecionar aba mais próxima, caso a aba atual esteja selecionada.
		if( tabHandler.tab.is( ".selected" ) ) {
			if( tabHandler.tab.next( ".mini-tab" ).length === 0 )
				tabHandler.tab.prev( ".mini-tab" ).trigger( "click" );
			else
				tabHandler.tab.next( ".mini-tab" ).trigger( "click" );
		}

		// Remover elementos da página.
		tabHandler.tab.remove();
		tabHandler.page.remove();

		// Atualizar lista de abas.
		$( "body" ).trigger( "updateTabsList" );

		return false;
	}).on( "click", ".mini-tab-page a.page-action.close", function() {
		navTabs[ ( $(this).closest( ".mini-tab-page" ).data( "tab-id" ) ) ].tab.find( ".close-act" ).trigger( "click" );
		return false;
	});

	/**
	 * Cancelar requisições Ajax.
	 */
	$( "body" ).on( "click", ".mini-tab-page .tab-preloader a.cancel-request", function() {
		var id = $(this).closest( ".mini-tab-page" ).data( "tab-id" );

		// Abortar requisição Ajax.
		if( navTabs[ ( id ) ] ) {
			if( navTabs[ ( id ) ].request !== null ) {
				navTabs[ ( id ) ].request.abort();
				navTabs[ ( id ) ].request = null;
			}

			// Fechar aba em branco após cancelamento.
			if( navTabs[ ( id ) ].tab.is( ".blank-tab" ) )
				navTabs[ ( id ) ].tab.find( ".close-act" ).trigger( "click" );
		}
	});

	/**
	 * Aplicar conceito de mini abas do sistema, para nevegação mais intuitiva.
	 */
	$( "body" ).on( "click loadDefaultPage", "a", function( e ) {
		var target = $(this),
			url = this.href,
			id = target.data( "tab-id" ) || url.hash(),
			itemId = target.data( "item-id" ) || id,
			pageId = target.closest( ".mini-tab-page" ).data( "tab-id" ),
			tabs = $( ".mini-tabs-itens" ),
			pages = $( ".mini-tabs-pages" ),
			isAllowedLink = ( this.hostname == location.hostname ) && !target.parent().is( "li.has-submenu" ),
			isInternalLink = target.closest( ".mini-tab-page" ).length > 0,
			loadInDefault = ( Object.keys( navTabs ).length < 2 && ( navTabs[ ( Object.keys( navTabs )[0] ) ].tab.is( ".is-default" ) ) );

		var miniTabEl = '<div class="mini-tab blank-tab">' +
							'<h4><span>' + gettext( "New tab" ) + '</span></h4>' +
							'<a href="#" data-tab-ignore="true" class="close-act"><i class="fas fa-times"></i></a>' +
						'</div>',
			pageTabEl = '<div class="mini-tab-page page-content">' +
							'<div class="easyform-preloader tab-preloader">' +
								'<a href="#" class="easyform-field type-button cancel-request" data-tab-ignore="true">' + gettext( "Cancel" ) + '</a>' +
							'</div>' +
						'</div>';

		// Ignorar elemento na navegação por abas.
		if( target.closest( ".easyform-table" ).length > 0 || target.data( "tab-ignore" ) || !target.attr( "href" ) || target.attr( "href" ) === "#" )
			return true;

		// Todos os links internos terão comportamento baseado no conceito de mini abas.
		if( isAllowedLink ) {
			// Definir aba e página para carregamento.
			if( !navTabs[ ( id ) ] ) {
				if( isInternalLink ) {
					id = pageId;
				}else if( loadInDefault || e.type === "loadDefaultPage" ) {
					id = navTabs[ ( Object.keys( navTabs )[0] ) ].id;
				}else{
					// Criar elementos da aba.
					$( miniTabEl ).data({ "item-id": itemId, "tab-id": id, "tab-url": url }).appendTo( tabs );
					$( pageTabEl ).data( "tab-id", id ).appendTo( pages );

					// Atualizar lista de abas.
					$( "body" ).trigger( "updateTabsList" );
				}
			}else if( navTabs[ ( id ) ].url === url ) {
				navTabs[ ( id ) ].tab.trigger( "click" );

				// Fechar aba, quando necessário.
				if( isInternalLink && id !== pageId )
					navTabs[ ( pageId ) ].tab.find( ".close-act" ).trigger( "click" );

				return false;
			}

			// Objeto da aba.
			var tabHandler = navTabs[ ( id ) ],
				preloader = tabHandler.page.find( ".tab-preloader" );

			// Cancelar requisição Ajax anterior.
			if( tabHandler.request !== null )
				tabHandler.request.abort();

			tabHandler.tab.trigger( "click" );
			preloader.stop().fadeIn( 200 );

			// Executar requisição Ajax para navegação.
			tabHandler.request = $.ajax({
				url: url,
				type: "post",
				data: {
					handler: "loadTab",
					backwardURL: ( isInternalLink ? tabHandler.url : "" )
				},
				dataType: "JSON"
			}).done( function( data ) {
				if( !data.status ) {
					alert( data.msg );
					return;
				}

				var isSelected = tabHandler.tab.is( ".selected" ),
					page = $( data.content ).toggle( isSelected ),
					tabId = page.data( "tab-id" ),
					tab = $( miniTabEl ).data({
							"tab-title": data.pageTitle,
							"tab-url": data.pageURL,
							"item-id": page.data( "item-id" ),
							"tab-id": tabId
						  }).prop( "title", page.data( "title" ) ).addClass( "no-cached" ).toggleClass( "selected", isSelected )
							.find( "h4 span" ).text( page.data( "title" ) ).end();

				// Verificar se a aba já existe e realizar tratamento.
				if( navTabs[ ( tabId ) ] ) {
					navTabs[ ( tabId ) ].tab.replaceWith( tab );

					if( navTabs[ ( tabId ) ].tab.is( ".blank-tab" ) || navTabs[ ( tabId ) ].url !== data.pageURL || isInternalLink )
						navTabs[ ( tabId ) ].page.replaceWith( page );

					// Fechar abas em branco.
					if( tabHandler.tab.is( ".blank-tab" ) ) {
						tabHandler.tab.remove();
						tabHandler.page.remove();
					}
				}else{
					tabHandler.tab.replaceWith( tab );
					tabHandler.page.replaceWith( page );
				}

				// Atualizar lista de abas.
				$( "body" ).trigger( "updateTabsList" );

				// Alterar histórico da aba.
				if( isSelected )
					tab.trigger( "click" );
			}).always( function( data, status ) {
				if( status !== "success" && status !== "abort" )
					alert( gettext( "We were unable to finish loading the page. Please check your internet connection." ) );

				// Esconder preloader.
				preloader.stop().fadeOut( 100 );
				$( "form.easyform, div.easyform-table" ).trigger( "loadContent" );
			});
		}

		// Todos os links externos serão desativados, a menos que seja usado o parâmetro "ignore".
		return false;
	});

	/**
	 * Auto salvamento para campos de texto, com 2 segundos de espera.
	 */
	var timerFieldsList = [];
	$( "body" ).on( "keyup", "form.easyform.auto-save .type-text, form.easyform.auto-save .type-textarea", function() {
		var field = $(this),
			name = field.data( "name" ) || "";

		if( name !== "" && timerFieldsList[ ( name ) ] )
			clearTimeout( timerFieldsList[ ( name ) ] );

		timerFieldsList[ ( name ) ] = setTimeout( function() {
			var isFocused = field.is( ":focus" );

			field.trigger( "blur" );

			if( isFocused )
				field.trigger( "focus" );
		}, 2000 );
	});

	/**
	 * Carregamento de imagens na biblioteca de imagens.
	 */
	$( "body" ).on( "click", ".easyform-modal.general-image-library a.load-more", function() {
		var handler = $(this),
			section = handler.closest( ".ef-modal-section" ),
			paged = parseInt( section.data( "page" ) || 2 ),
			url = section.data( "url" );

		handler.addClass( "loading" );

		// Cancelar requisição anterior.
		if( section.data( "request" ) && section.data( "request" ) !== null ) {
			section.data( "request" ).abort();
			section.data( "request", null );
		}

		var request = $.ajax({
			url: ( url + "&paged=" + paged )
		}).always( function( data, status ) {
			handler.removeClass( "loading" );

			if( status === "success" && data !== "" ) {
				section.data( "page", ( paged + 1 ) );
				section.find( "ul.image-select-list" ).append( $( data ).find( "li" ) );
			}else{
				handler.remove();
			}
		});

		// Armazenar requisição.
		section.data( "request", request );

		return false;
	});

	/**
	 * Adicionar página aos favoritos.
	 */
	$( "body" ).on( "click", ".page-head .page-action.favorite", function() {
		var handler = $(this);

		// Remover página dos favoritos.
		if( handler.is( ".selected" ) ) {
			confirm({
				message: gettext( "Do you really want to remove this page from your favorites?" ),
				onSubmit: function() {
					$.ajax({
						type: "POST",
						dataType: "JSON",
						data: {
							handler: "removeFavorite",
							menu: handler.data( "menu" ),
							id: handler.data( "id" )
						}
					}).always( function( data, status ) {
						if( status !== "success" || !data.status || !data.content ) {
							alert( gettext( "The request could not be completed. Please try again." ) );
						}else{
							alert( gettext( "This page has been successfully removed from favorites." ) );
							handler.removeClass( "selected" );
						}
					});
				}
			});
		}
		// Salvar novo favorito.
		else{
			var modal = new easyModal(),
				modalContent = $( '<div class="fav-options-wrapper">' +
					'<div class="fav-folder-select">' +
						'<label>' +
							'<span>' + gettext( "Select the folder:" ) + '</span><br />' +
							'<select class="easyform-field type-select" disabled name="folderId">' + 
								'<option value="">' + gettext( "Create new folder" ) + '</option>' +
							'</select>' +
						'</label>' +
					'</div>' +
					'<div class="fav-folder-create">' +
						'<label>' +
							'<span>' + gettext( "New folder name:" ) + '</span><br />' +
							'<input type="text" class="easyform-field type-text" name="folderName" autocomplete="off" />' +
						'</label>' +
					'</div>' +
				'</div>' );

			modal.general({
				minimizable: false,
				title: gettext( "Save new bookmark" ),
				class: "favorite-options",
				icon: "star",
				onSubmit: function( favHandler, favModal ) {
					$.ajax({
						type: "POST",
						dataType: "JSON",
						data: favModal.find( ".fav-options-wrapper" ).find( "input, select" ).serialize() + "&handler=addFavorite&menu=" + handler.data( "menu" ) + "&id=" + handler.data( "id" )
					}).always( function( data, status ) {
						if( status !== "success" || !data.status || !data.content )
							alert( gettext( "The request could not be completed. Please try again." ) );
						else
							handler.addClass( "selected" );
					});
				}
			}).find( ".ef-modal-content" ).append( modalContent );

			// Carregar pastas de favoritos.
			$.ajax({
				type: "POST",
				dataType: "JSON",
				data: {
					handler: "getFFolderList"
				}
			}).always( function( data, status ) {
				modalContent.find( ".fav-folder-select select" ).prop( "disabled", false );

				if( status !== "success" || !data.status || !data.content ) {
					alert( gettext( "The request could not be completed. Please try again." ) );
				}else{
					for( var folder of data.content )
						modalContent.find( ".fav-folder-select select" ).append( '<option value="' + folder.ffolderid + '">' + ( '&emsp;'.repeat( folder.level - 1 ) ) + folder.ffoldername + '</option>' );
				}
			});
		}

		return false;
	});
});

/**
 * Filtro de elementos pelo atributo "data".
 */
$.fn.getByData = function( dataId, value ) {
	return this.filter( function() {
		return $(this).data( dataId ) == value;
	});
}

/**
 * Obter o nome do sistema a  partir de metatag.
 */
var getSystemName = function() {
	var metaTag = document.querySelector( 'meta[name="system-name"]' );

	if( metaTag )
		return metaTag.content;

	return "";
};


/**
 * Codificação simples para strings.
 */
String.prototype.hash = function() {
	var self = this,
		range = Array( this.length );

	for( var i = 0; i < this.length; i++ )
		range[ ( i ) ] = i;

	return "h" + Array.prototype.map.call( range, function( i ) {
		return self.charCodeAt( i ).toString( 16 );
	}).join( "" );
}