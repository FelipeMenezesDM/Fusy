<?php
/**
 * Arquivo de métodos de retorno Ajax do Fusy Framework.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

/**
 * Obter conteúdo de páginas do sistema para retorno via Ajax.
 * @return string
 */
function handlerLoadTab() {
	ob_start();
	getSetupPage( "index" );
	$content = ob_get_contents();
	ob_end_clean();

	return $content;
}

/**
 * Remover favorito do usuário.
 * @return boolean
 */
function handlerRemoveFavorite() {
	if( !isset( $_POST[ "id" ] ) || !isset( $_POST[ "menu" ] ) )
		return false;

	$ids = array();
	$favorite = new Favorite();
	$favorite->get( array(
		array(
			"key"	=> "favoriteSlug",
			"value"	=> $_POST[ "id" ]
		),
		array(
			"key"	=> "favoriteMenu",
			"value"	=> $_POST[ "menu" ]
		),
		array(
			"key"	=> "favoriteUserId",
			"value"	=> getUserID()
		)
	) );

	# Listar todos os favoritos do usuário.
	do {
		$ids[] = $favorite->getFavoriteID();
	}while( $favorite->next() );

	$favorite = new Favorite();

	if( $favorite->delete( $ids ) )
		return true;

	return false;
}

/**
 * Salvar novo favorito do usuário.
 * @return boolean
 */
function handlerAddFavorite() {
	if( !isset( $_POST[ "id" ] ) || !isset( $_POST[ "menu" ] ) )
		return false;

	$menuObj = getMenu( $_POST[ "menu" ], false );
	$item = $menuObj->getItem( $_POST[ "id" ] );

	if( $item ) {
		$folderId = isset( $_POST[ "folderId" ] ) ? trim( $_POST[ "folderId" ] ) : "";
		$folderName = isset( $_POST[ "folderName" ] ) ? trim( $_POST[ "folderName" ] ) : "";

		# Verificar se a pasta seleciona existe e pertence ao usuário.
		if( !empty( $folderId ) ) {
			$ffolder = new FavoriteFolder();
			$ffolder->get( array(
				array(
					"key"	=> "ffolderId",
					"value"	=> $folderId
				),
				array(
					"key"	=> "ffolderUserId",
					"value"	=> getUserID()
				)
			) );

			if( is_null( $ffolder->getFFolderId() ) )
				return false;
		}

		# Criação de nova pasta.
		if( !empty( $folderName ) ) {
			$uuid = uuid();
			$ffolder = new FavoriteFolder();
			$ffolder->setFFolderId( $uuid );
			$ffolder->setFFolderName( $folderName );
			$ffolder->setFFolderUserId( getUserID() );
			$ffolder->setFFolderParentId( ( empty( $folderId ) ? null : $folderId ) );

			if( !$ffolder->post() )
				return false;

			# Obter pasta criada.
			$folderId = $uuid;
		}

		# Verificar se o favorito já existe.
		if( isFavorite( $item[ "id" ], $menuObj->getID() ) )
			return false;

		# Salvar favorito.
		$favorite = new Favorite();
		$favorite->setFavoriteSlug( $item[ "id" ] );
		$favorite->setFavoriteMenu( $menuObj->getID() );
		$favorite->setFavoriteUserId( getUserID() );
		$favorite->setFavoriteFolderId( ( empty( $folderId ) ? null : $folderId ) );

		if( $favorite->post() )
			return true;
	}

	return false;
}

/**
 * Obter lista de pastas de favoritos do usuário.
 * @return array
 */
function handlerGetFFolderList() {
	return getFavoriteFolderCascade();
}

/**
 * Obter lista de imagens de perfil carregadas do usuário.
 * @return string
 */
function handlerGetUserProfileImages() {
	# Atualizar imagem selecionada na biblioteca.
	if( isset( $_REQUEST[ "userPPicture" ] ) && !empty( $_REQUEST[ "userPPicture" ] ) ) {
		$newPPicture = $_REQUEST[ "userPPicture" ];

		$person = new Person();
		$person->setPersonPPictureId( $newPPicture );
		$person->setPersonId( getUserInfo( "personid" ) );

		# Retornar informações da imagem salva.
		if( $person->put() ) {
			$picture = new ProfilePicture();

			if( $picture->getById( $newPPicture ) ) {
				try{ $position = json_decode( $picture->getPPictureSetts(), true ); }
				catch( Exception $e ){ $position = array(); }

				$crop = new easyCrop( $picture->getPPictureAttach(), array_merge( $position, array( "background" => "#FFFFFF", "base64" => true ) ) );

				return json_encode( array(
					"status"	=> true,
					"thumbnail"	=> $crop->getBase64( 180 ),
					"original"	=> $crop->getOriginalBase64(),
					"setts"		=> $position
				) );
			}
		}

		return json_encode( array( "status" => false ) );
	}

	$paged = max( 1, ( isset( $_GET[ "paged" ] ) ? (int) $_GET[ "paged" ] : 1 ) );
	$perPage = 15;

	# Obter lista de imagens.
	$images = ProfilePicture::_get( array(
		"per_page"		=> $perPage,
		"paged"			=> $paged,
		"meta_query"	=> array( array( "key" => "ppicturePersonId", "value" => getUserInfo( "personid" ) ) ),
		"order_by"		=> array( "ppictureCreation" => "DESC" )
	) );

	# Criar lista de imagens para seleção.
	$content = new imageSelectList( array(
		"name"		=> "userPPicture",
		"size"		=> 150,
		"form"		=> ( $paged > 1 ? false : true ),
		"current"	=> getUserInfo( "personPPictureId" )
	) );

	# Criar itens para seleção.
	if( $images ) {
		foreach( $images[ "results" ] as $image ) {
			$content->addImage( array(
				"id"		=> $image[ "ppictureid" ],
				"position"	=> $image[ "ppicturesetts" ],
				"attach"	=> $image[ "ppictureattach" ]
			) );
		}
	}else{
		return "";
	}

	$imagesList = $content->getImages( false );

	if( empty( $imagesList ) && $paged === 1 )
		return "";

	# Exibir botão para carregamento de imagens.
	if( $paged === 1 && $images[ "total" ] > $perPage )
		$imagesList .= '<a href="#" class="easyform-field type-button border load-more">' . gettext( "Load more" ) . '</a>';
	
	return $imagesList;
}