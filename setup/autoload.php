<?php
/**
 * Autoload de classes do Fusy Framework.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

spl_autoload_register( function( $class ) {
	$path = __DIR__ . DIRECTORY_SEPARATOR . "src" . DIRECTORY_SEPARATOR;
	$pathEntities = $path . "entities" . DIRECTORY_SEPARATOR;

	if( file_exists( $path . $class . ".class.php" ) )
		require_once( $path . $class . ".class.php" );
	elseif( file_exists( $pathEntities . $class . ".class.php" ) )
		require_once( $pathEntities . $class . ".class.php" );
	else
		throw new Exception( "Required class {$class} does not exist. Please contact your system administrator." );
});