<?php
/**
 * Arquivo de tratamento para requisições Ajax.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

# Incluir funções de retorno Ajax.
getSetupPage( "functions-ajax" );

# Verificar se o método de manipulação está disponível.
if( isset( $_REQUEST[ "handler" ] ) && function_exists( "handler" . trim( $_REQUEST[ "handler" ] ) ) )
	$return = array(
		"status"	=> true,
		"msg"		=> "",
		"content"	=> call_user_func( "handler" . trim( $_REQUEST[ "handler" ] ) )
	);
else
	$return = array(
		"status"	=> false,
		"msg"		=> gettext( "Invalid request, as the required return method could not be found." ),
		"content"	=> null
	);

# Definir título e URL da página.
$return[ "pageTitle" ] = getPageTitle();
$return[ "pageURL" ] = getPageURL();

# Imprimir resultado. Obrigatoriamente retornado como JSON.
if( !isset( $_REQUEST[ "fullcontent" ] ) ) {
	echo json_encode( $return );
}else{
	if( $return[ "status" ] )
		echo $return[ "content" ];
	else
		echo $return[ "msg" ];
}
