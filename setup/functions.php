<?php
/**
 * Arquivo de funções gerais do Fusy Framework.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

/**
 * Identificar requisição Ajax.
 * @return boolean
 */
function isAjax() {
	return easyForm::isAjax();
}

/**
 * Idenficar modo de impressão de relatórios.
 * @return boolean
 */
function isPrinting() {
	return easyTable::isExportMode();
}

/**
 * Identificar se a transação pertence ao setup.
 * @return boolean
 */
function isSetup() {
	return !is_null( getRequestParam( "setup" ) );
}

/**
 * Obter todas as configurações salvas do sistema.
 * @return array
 */
function getSettings() {
	$settings = array();
	$systemSetting = new Setting();
	$systemSetting->get();

	do{
		try {
			try{
				$content = json_decode( $systemSetting->getContent(), true );
			}catch( Exception $e ) {
				$content = unserialize( $systemSetting->getContent() );
			}
		}catch( Exception $e ) {
			$content = null;
		}

		if( !is_null( $content ) )
			$systemSetting->setContent( $content );

		$settings[ strtolower( $systemSetting->getSlug() ) ] = $systemSetting->getContent();
	}while( $systemSetting->next() );

	return $settings;
}

/**
 * Obter valor de configuração do sistema.
 * @param  string $slug Identificador da configuração.
 * @return *
 */
function getSetting( $slug ) {
	global $settings;
	$slug = strtolower( trim( $slug ) );

	if( isset( $settings[ ( $slug ) ] ) )
		return $settings[ ( $slug ) ];
}

/**
 * Incluir arquivos com tratamento para verificar se ele existe no sistema.
 * @param  string  $file Localização do arquivo no diretório do Framework.
 * @return boolean
 */
function _require( $file ) {
	$file = _file_exists( $file );

	# Verificar se o arquivo existe no servidor;
	if( $file )
		require_once( $file );

	return $file;
}

/**
 * Alias para _require.
 * @param  string  $file 
 * @return boolean
 */
function _include( $file ) {
	return _require( $file );
}

/**
 * Verificar se o arquivo existe no servidor, com tratamento para separador de diretórios.
 * @param  string $file Localização do arquivo no diretório do Framework.
 * @return string
 */
function _file_exists( $file ) {
	$file = str_replace( array( "/", "\\" ), SEP, $file );

	# Verificar se o arquivo existe no servidor;
	if( !file_exists( $file ) )
		return false;

	return $file;
}

/**
 * Leitura do arquivo do servidor, com tratamento para separador de diretórios.
 * @param  string $file Localização do arquivo no diretório do Framework.
 * @param  string $mode Modo de abertura do arquivo.
 * @return object
 */
function _fopen( $file, $mode ) {
	return fopen( str_replace( array( "/", "\\" ), SEP, $file ), $mode );
}

/**
 * Leitura de contéudo do arquivo do servidor, com tratamento para separador de diretórios.
 * @param  string $file Localização do arquivo no diretório do Framework.
 * @return string
 */
function _file_get_contents( $file ) {
	return file_get_contents( $file );
}

/**
 * Remover um diretório não vazio do framework.
 * @param string $dir Localização do diretório.
 */
function _rmdir( $dir ) {
	$dir = str_replace( array( "/", "\\" ), SEP, $dir );
	$files = array_diff( scandir( $dir ), array( ".", ".." ) ); 

	foreach( $files as $file )
		( is_dir( "$dir/$file" ) ) ? _rmdir( "$dir/$file" ) : unlink( "$dir/$file" ); 

    return rmdir($dir); 
}

/**
 * Incluir arquivo de um formulário.
 * @param  string  $formSlug Nome do formulário, sem extensão.
 * @return boolean
 */
function getForm( $formSlug ) {
	return _include( "./content/forms/${formSlug}.form.php" );
}

/**
 * Incluir arquivo de uma página.
 * @param  string  $pageSlug Nome da página, sem extensão.
 * @return boolean
 */
function getPage( $pageSlug ) {
	return _include( "./content/pages/${pageSlug}.php" );
}

/**
 * Incluir arquivo de uma página do setup.
 * @param  string  $pageSlug Nome da página, sem extensão.
 * @return boolean
 */
function getSetupPage( $pageSlug ) {
	return _include( "./setup/${pageSlug}.php" );
}

/**
 * Incluir arquivo de composição do layout.
 * @param  string  $slug Nome do arquivo para inclusão, sem extensão.
 * @return boolean
 */
function getTemplatePart( $slug ) {
	return _include( "./content/includes/${slug}.php" );
}

/**
 * Método padrão para criptografia de senhas.
 * @param  string $value Valor para criptografia.
 * @return string
 */
function defaultPasswordEncrypt( $value ) {
	return password_hash( $value, PASSWORD_DEFAULT );
}

/**
 * Validar senha a partir de hash.
 * @param  string  $password Senha para comparação.
 * @param  string  $hash     Hash para comparação.
 * @return boolean
 */
function defaultPasswordHashValidate( $password, $hash ) {
	return password_verify( $password, $hash );
}

/**
 * Método padrão para criptografia de dados.
 * @param  string $value Valor para criptografia.
 * @return string
 */
function defaultDataEncrypt( $value ) {
	$method = "AES-256-CBC";
	$iv = random_bytes( openssl_cipher_iv_length( $method ) );
	return base64_encode( $iv . openssl_encrypt( $value, $method, "DEFAULT_ENCRYPT_FUSY", OPENSSL_RAW_DATA, $iv ) );
}

/**
 * Método para descriptografar dados equivalente ao método padrão de criptografia.
 * @param  string $value Valor para descriptografar.
 * @return string
 */
function defaultDataDecrypt( $value ) {
	$value = base64_decode( $value );
	$method = "AES-256-CBC";
	$iv = mb_substr( $value, 0, openssl_cipher_iv_length( $method ), "8bit" );
	$value = mb_substr( $value, openssl_cipher_iv_length( $method ), null, "8bit" );

	return openssl_decrypt( $value, $method, "DEFAULT_ENCRYPT_FUSY", OPENSSL_RAW_DATA, $iv );
}

/**
 * Gerador de UUIDs.
 * @return string
 */
function uuid() {
	return md5( uniqid( rand(), true ) );
}

/**
 * Obter o endereço IP do usuário.
 * @return string
 */
function getUserIP() {
	if( isset( $_SERVER[ "HTTP_X_FORWARDED_FOR" ] ) && $_SERVER[ "HTTP_X_FORWARTDED_FOR" ] != "" )
	    return $_SERVER[ "HTTP_X_FORWARDED_FOR" ];
	else
		return $_SERVER[ "REMOTE_ADDR" ];
}

/**
 * Obter lista de idiomas disponíveis do sistema.
 * @param  string $lang Código do idioma ou falso, para retornar todas os idiomas suportados.
 * @return array
 */
function getAvailableLanguages( $lang = false ) {
	$languages = array(
		array( "value" => "en_US", "title" => "English (US)" ),
		array( "value" => "pt_BR", "title" => "Português (Brasil)" )
	);

	if( $lang ) {
		foreach( $languages as $language ) {
			if( strtolower( $lang ) === strtolower( $language[ "value" ] ) )
				return $language[ "title" ];
		}

		return $lang;
	}

	return $languages;
}

/**
 * Obter lista de gêneros válida.
 * @return array
 */
function getAvailableGenders() {
	return array(
		array( "title" => gettext( "Female" ), "value" => "F" ),
		array( "title" => gettext( "Male" ), "value" => "M" ),
		array( "title" => gettext( "Other" ), "value" => "O" )
	);
}

/**
 * Obter lista de estado civil válida.
 * @return array
 */
function getAvailableMaritalStatus() {
	return array(
		array( "title" => gettext( "Select your martial status" ), "value" => "" ),
		array( "title" => gettext( "Single" ), "value" => "1" ),
		array( "title" => gettext( "Married" ), "value" => "2" ),
		array( "title" => gettext( "Divorced" ), "value" => "3" ),
		array( "title" => gettext( "Widower/widow" ), "value" => "4" )
	);
}

/**
 * Obter lista de raças válida.
 * @return array
 */
function getAvailableBreeds() {
	return array(
		array( "title" => gettext( "Select your breed" ), "value" => "" ),
		array( "title" => gettext( "White" ), "value" => "1" ),
		array( "title" => gettext( "Black" ), "value" => "2" ),
		array( "title" => gettext( "Brown" ), "value" => "3" ),
		array( "title" => gettext( "Yellow" ), "value" => "4" ),
		array( "title" => gettext( "Indigenous" ), "value" => "5" )
	);
}

/**
 * Obter lista de tipos sanguíneos válida.
 * @return array
 */
function getAvailableBloodTypes() {
	return array(
		array( "title" => gettext( "Select your blood type" ), "value" => "" ),
		array( "title" => gettext( "A+" ), "value" => "1" ),
		array( "title" => gettext( "A-" ), "value" => "2" ),
		array( "title" => gettext( "B+" ), "value" => "3" ),
		array( "title" => gettext( "B-" ), "value" => "4" ),
		array( "title" => gettext( "AB+" ), "value" => "5" ),
		array( "title" => gettext( "AB-" ), "value" => "6" ),
		array( "title" => gettext( "O+" ), "value" => "7" ),
		array( "title" => gettext( "O-" ), "value" => "8" )
	);
}

/**
 * Obter definições de um campo a partir de seu objeto.
 * @param  string $tab       Identificador da tabela.
 * @param  string $fieldName Identificador do campo.
 * @param  array  $setts     Lista de definições complementares.
 * @return array
 */
function getObjectField( $tab, $fieldName, $setts = array() ) {
	$table = smoothModeler::getTable( $tab );
	$field = $table->getColumnByName( $fieldName );
	$setts = array_merge( array_merge( array(
		"name"		=> $field[ "name" ],
		"max"		=> $field[ "length" ],
		"required"	=> $field[ "not_null" ],
		"entity"	=> $table->getID()
	), ( isset( $field[ "props" ] ) ? arrayKeyHandler( $field[ "props" ] ) : array() ) ), arrayKeyHandler( $setts ) );

	# Para campos com valores únicos, implementar validação padrão automaticamente.
	if( $field[ "unique" ] && !isset( $setts[ "onsuccess" ] ) )
		$setts[ "onsuccess" ] = "validateDuplicateRecord";

	# Obter listas a partir de funções.
	if( isset( $setts[ "options" ] ) && is_string( $setts[ "options" ] ) && function_exists( $setts[ "options" ] ) )
		$setts[ "options" ] = $setts[ "options" ]();

	return $setts;
}

/**
 * Obter definição única de um campo a partir de seu objeto.
 * @param  string $tab       Identificador da tabela.
 * @param  string $fieldName Identificador do campo.
 * @param  string $param     Identificador do parâmetro.
 * @return *
 */
function getObjectFieldParam( $tab, $fieldName, $param ) {
	$table = smoothModeler::getTable( $tab );
	$column = $table->getColumnByName( $fieldName );
	$param = strtolower( trim( $param ) );

	if( isset( $column[ ( $param ) ] ) ) {
		$value = $column[ ( $param ) ];

		if( is_string( $value ) && function_exists( $value ) )
			$value = $value();

		return $value;
	}

	return null;
}

/**
 * Obter idioma do sistema.
 * @return string
 */
function getSystemLanguage() {
	return isset( $_SESSION[ "system_language" ] ) && !empty( $_SESSION[ "system_language" ] ) ? $_SESSION[ "system_language" ] : false;
}

/**
 * Obter fuso horário do sistema.
 * @return string
 */
function getSystemTimezone() {
	return isset( $_SESSION[ "system_timezone" ] ) && !empty( $_SESSION[ "system_timezone" ] ) ? $_SESSION[ "system_timezone" ] : false;
}

/**
 * Definir idioma do sistema.
 * @param string $language Idioma.
 */
function setSystemLanguage( $language ) {
	$_SESSION[ "system_language" ] = $language;
}

/**
 * Definir fuso horário do sistema.
 * @param string $timezone Fuso horário.
 */
function setSystemTimezone( $timezone ) {
	$_SESSION[ "system_timezone" ] = $timezone;
}

/**
 * Método estático para tratamento de chaves de array.
 * @param  array $array Lista para tratamento.
 * @param  array $case  Transformação do texto da chave.
 * @return array
 */
function arrayKeyHandler( $array, $case = CASE_LOWER ) {
	return easyValidate::arrayKeyHandler( $array, $case );
}

/**
 * Obter URL da aplicação.
 * @return string
 */
function getAppURL() {
	return easyTable::getAppURL();
}

/**
 * Obter URL da página atual.
 * @return string
 */
function getPageURL() {
	return easyTable::getPageURL();
}

/**
 * Obter protocolo atual do sistema (HTTP ou HTTPS).
 * @return string
 */
function getServerProtocol() {
	return easyTable::getServerProtocol();
}

/**
 * Adicionar argumento em uma URL usando o método GET.
 * @param  string $query Chave do argumento.
 * @param  string $arg   Valor do argumento.
 * @param  string $url   URL base.
 * @return string
 */
function addQueryArg( $query, $arg, $url = ""  ) {
	return easyTable::addQueryArg( $query, $arg, $url );
}

/**
 * Adicionar múltiplos argumentos em um URL usando o método GET.
 * @param array  $queries Argumentos com chave e valor em vetor.
 * @param string $url     URL base.
 * @return string
 */
function addQueryArgs( $queries = array(), $url = "" ) {
	return easyTable::addQueryArgs( $queries, $url );
}

/**
 * Remove argumentos (um ou vários) GET de uma URL.
 * @param  array  $query Argumentos para remoção.
 * @param  string $url   URL base.
 * @return string
 */
function removeQueryArg( $query, $url = "" ) {
	return easyTable::removeQueryArg( $query, $url );
}

/**
 * Adicionar ou remover arqumento de forma alternada.
 * @param  string $query        Argumento para altenar.
 * @param  string $url          URL base.
 * @param  string $defaultValue Valor padrão do argumento.
 * @return string
 */
function toggleQueryArg( $query, $url, $defaultValue = "1" ) {
	return easyTable::toggleQueryArg( $query, $url, $defaultValue );
}

/**
 * Remover itens de uma lista a partir de uma chave.
 * @param  array $items     Lista de itens para remoção.
 * @param  array $arraylist Ponteiro para a lista de itens base.
 * @return array
 */
function removeArrayItems( $items, & $arraylist ) {
	return easyTable::removeArrayItems( $items, $arraylist );
}

/**
 * Obter valor de parâmetro no objeto da requisição.
 * @param  string  $query         Query para busca no objeto de requisição.
 * @param  string  $method        Objeto de requisição.
 * @param  boolean $caseSensitive Definir se a busca deve ser case sensitive.
 * @return string
 */
function getRequestParam( $query, $method = "GET", $caseSensitive = false ) {
	return easyTable::getRequestParam( $query, $method, $caseSensitive, false );
}

/**
 * Redirecionamento de URL sem cache de conteúdo.
 * @param string $url URL para redirecionamento.
 */
function redirectWithoutCache( $url ) {
	easyValidate::redirectWithoutCache( $url );
}

/**
 * Inclusão de páginas do sistema.
 */
function pages() {
	# Páginas de controle do usuário.
	if( !empty( $page = strtolower( trim( getRequestParam( "user" ) ) ) ) )
		( !getPage( "user-${page}" ) ? getPage( "404" ) : true );
	# Identificar requisições Ajax automaticamente.
	elseif( isAjax() && isset( $_REQUEST[ "handler" ] ) )
		getSetupPage( "ajax" );
	# Definir página central do setup.
	elseif( isSetup() )
		getSetupPage( "index" );
	# Definir página inicial, de acordo com as configurações do sistema.
	elseif( getSetting( "home_page" ) === "default_home_page" )
		getPage( "home" );
	# Definir a página de login como página padrão.
	else
		getPage( "user-signin" );
}

/**
 * Obter cabeçalho para páginas comuns do usuário.
 * @param string $header Cabeçalho para inclusão.
 */
function getHeader( $header = "" ) {
	getTemplatePart( ( !empty( $header ) ? $header . "-" : "" ) . "header" );
}

/**
 * Obter rodapé para páginas comuns do usuário.
 * @param string $footer Rodapé para inclusão
 */
function getFooter( $footer = "" ) {
	getTemplatePart( ( !empty( $footer ) ? $footer . "-" : "" ) . "footer" );
}

/**
 * Obter o título global para uma página.
 * @return string
 */
function getPageTitle() {
	global $pageTitle;
	return $pageTitle;
}

/**
 * Definir título global para a página.
 * @param string $title Novo título para a página.
 */
function setPageTitle( $title ) {
	global $pageTitle;
	$pageTitle = $title;
}

/**
 * Verificar se o usuário possui permissão de acesso.
 * @param  string  $role ID da permissão de acesso.
 * @return boolean
 */
function getUserRole( $role ) {
	return true;
}

/**
 * Definir nova folha de estilos.
 * @param string $id           ID do grupo.
 * @param string $src          Link para o ativo.
 * @param array  $dependencies Lista de dependências do ativo.
 * @param string $version      Versão do arquivo.
 */
function setStyle( $id, $src, $dependencies = array(), $version = "" ) {
	assetsController::setStyle( $id, $src, $dependencies, $version );
}

/**
 * Definir novo script.
 * @param string $id           ID do grupo.
 * @param string $src          Link para o ativo.
 * @param array  $dependencies Lista de dependências do ativo.
 * @param string $version      Versão do arquivo.
 */
function setScript( $id, $src, $dependencies = array(), $version = "" ) {
	assetsController::setScript( $id, $src, $dependencies, $version );
}

/**
 * Imprimir todas as folhas de estilo.
 * @param string $id ID do grupo, caso seja necessário imprimir apenas um grupo de estilos definido.
 */
function getStyles( $id = "" ) {
	assetsController::getStyles( $id );
}

/**
* Imprimir todas os scripts.
* @param string $id ID do grupo, caso seja necessário imprimir apenas um grupo de scripts definido.
*/
function getScripts( $id = "" ) {
	assetsController::getScripts( $id );
}

/**
 * Verificar sessão autenticada do usuário.
 * @param  boolean $expected Valor experado em relação à sessão do usuário. Quando TRUE, é esperado que a sessão exista. Quando FALSE, é esperado que não exista.
 * @return void
 */
function checkAuthAccess( $expected = true ) {
	global $currentUser;

	# Redirecionar quando a sessão de usuário existe, mas a página espera que ela não exista. Ex.: Página de login.
	if( $expected === false && !is_null( $currentUser ) ) {
		$url = getAppURL() . "?setup";

		if( !is_null( getRequestParam( "redirect" ) ) )
			$url = urldecode( getRequestParam( "redirect" ) );

		redirectWithoutCache( $url );
	}
	# Redirecionar quando a sessão de usuário não existe, mas a página espera que ela exista. Ex: Página de administração.
	elseif( $expected === true && is_null( $currentUser ) ) {
		redirectWithoutCache( addQueryArgs( array( "user" => "signin", "redirect" => urlencode( getPageURL() ) ), getAppURL() ) );
	}
}

/**
 * Obter URL de estilos.
 * @param  string $file Nome do arquivo para carregamento.
 * @return string
 */
function getStylesURI( $file = "" ) {
	$uri = getAppURL() . "assets/css/";

	if( !empty( $file ) )
		$uri .= "${file}.css";

	return $uri;
}

/**
 * Obter URL de scripts.
 * @param  string $file Nome do arquivo para carregamento.
 * @param  string $ext  Extensão do arquivo, caso seja diferente de JS.
 * @return string
 */
function getScriptsURI( $file = "", $ext = "" ) {
	$uri = getAppURL() . "assets/js/";

	if( !empty( $file ) )
		$uri .= "${file}." . ( empty( $ext ) ? "js" : $ext );

	return $uri;
}

/**
 * Obter valor de propriedade do usuário autenticado na sessão.
 * @param  string $property Nome da propriedade.
 * @return string
 */
function getUserInfo( $property, $userId = null ) {
	if( is_null( $userId ) ) {
		global $currentUser;
		return $currentUser->getProperty( $property );
	}else{
		$user = new User();
		$user->getById( $userId );
		return $user->getProperty( $property );
	}
}

/**
 * Obter o ID do usuário autenticado na sessão.
 * @return integer
 */
function getUserID() {
	return getUserInfo( "userId" );
}

/**
 * Obter nome de exibição do usuário autenticado na sessão.
 * @return string
 */
function getUserDisplayName( $userId = null ) {
	$nickname = getUserInfo( "nickname", $userId );
	$fullname = getUserInfo( "fullname", $userId );

	if( empty( $nickname ) )
		return $fullname;

	return $nickname;
}

/**
 * Obter imagem do usuário.
 * @param  integer $size   Tamanho da imagem.
 * @param  boolean $tag    Define se a tag <img /> deve ser incluída no retorno. 
 * @param  integer $userId ID de usuário para obter imagem.
 * @return string
 */
function getUserImage( $size = 300, $tag = true, $userId = null ) {
	if( !$image = getUserSavedImage( $userId ) ) {
		$image = getLetterImage( getUserDisplayName( $userId ), getUserMeta( "color", $userId ), $size );
	}else{
		$crop = new easyCrop( $image[ "attach" ], array_merge( $image[ "position" ], array( "background" => "#FFFFFF", "base64" => true ) ) );
		$image = $crop->getBase64( $size );
	}

	if( $tag )
		return '<img src="' . $image . '" alt="" />';

	return $image;
}

/**
 * Obter imagem de perfil cadastrada do usuário.
 * @param  integer $userId ID de usuário para obter imagem.
 * @return string
 */
function getUserSavedImage( $userId = null ) {
	if( is_null( $userId ) )
		$userId = getUserID();

	$profilePicture = new ProfilePicture();
	$image = $profilePicture->get(
		array(
			array( "key" => "userId", "value" => $userId ),
			array( "key" => "personPPictureId", "column" => "ppictureId" )
		),
		array(),
		array( "ppictureattach", "ppicturesetts" )
	);

	if( $image ) {
		return array(
			"attach"	=> $profilePicture->getPPictureAttach(),
			"position"	=> ( !empty( $profilePicture->getPPictureSetts() ) ? json_decode( $profilePicture->getPPictureSetts(), true ) : array() )
		);
	}

	return null;
}

/**
 * Obter vetor a partir de letra.
 * @param  string  $letter Palavra ou letra para construção do vetor.
 * @param  string  $color  Cor de fundo do vetor.
 * @param  integer $size   Dimensão do vetor.
 * @return string
 */
function getLetterImage( $letter, $color, $size ) {
	$letterImage = new letterVector( substr( $letter, 0, 1 ), $color, $size );
	return $letterImage->getVector();
}

/**
 * Obter informações genéricas de usuários.
 * @param  array   $metaIds Lista de slugs identificadores das informações do usuário.
 * @param  integer $userId  ID do usuário.
 * @return array
 */
function getUserMeta( $metaIds, $userId = null ) {
	if( is_null( $userId ) )
		$userId = getUserID();

	if( !is_array( $metaIds ) )
		$metaIds = (array) trim( $metaIds );

	$metaIds = array_filter( array_map( "trim", array_values( $metaIds ) ) );

	# Obter informações do usuário a partir do ID.
	$userMeta = new UserMeta();
	$userMeta->get( array(
		array( "key" => "metaUserId", "value" => $userId ),
		array( "key" => "metaSlug", "compare" => "IN", "value" => $metaIds )
	), array(), array( "metaslug", "metacontent" ) );

	# Validar retorno.
	$userMetaValues = array();

	# Listar informações do usuário.
	do{
		try {
			try {
				$metaContent = json_decode( $userMeta->getMetaContent(), true );
			}catch( Exception $e ) {
				$metaContent = unserialize( $userMeta->getMetaContent() );
			}
		}catch( Exception $e ) {
			$metaContent = null;
		}

		if( !is_null( $metaContent ) )
			$userMeta->setMetaContent( $metaContent );

		$userMetaValues[ strtolower( $userMeta->getMetaSlug() ) ] = $userMeta->getMetaContent();
	}while( $userMeta->next() );

	# Lista de retorno.
	$return = array();

	# Listar informações validas.
	foreach( $metaIds as $metaId )
		$return[ ( $metaId ) ] = ( isset( $userMetaValues[ strtolower( $metaId ) ] ) ? $userMetaValues[ strtolower( $metaId ) ] : null );

	if( count( $metaIds ) === 1 )
		return $return[ ( $metaIds[0] ) ];

	return $return;
}

/**
 * Obter item de uma lista de dados genéricos do usuário.
 * @param  string  $meta   Identificador do dado genérico.
 * @param  string  $item   Identificador do item da lista.
 * @param  integer $userId ID do usuário.
 * @return string
 */
function getUserMetaItem( $meta, $item, $userId = null ) {
	if( is_array( $meta ) )
		return null;

	$userMeta = getUserMeta( $meta, $userId );
	$item = strtolower( trim( $item ) );

	if( !is_null( $userMeta ) && is_array( $userMeta ) ) {
		$userMeta = arrayKeyHandler( $userMeta );

		return isset( $userMeta[ ( $item ) ] ) ? $userMeta[ ( $item ) ] : null;
	}

	return null;
}

/**
 * Atualizar ou inserir dado genérico do usuário.
 * @param  string  $meta     Identificador do dado genérico.
 * @param  string  $value    Valor do dado genérico.
 * @param  string  $metaType Tipo do dado genérico.
 * @param  integer $userId   ID do usuário.
 */
function putUserMeta( $meta, $value, $metaType, $userId = null ) {
	if( is_null( $userId ) )
		$userId = getUserID();

	$metaType = strtoupper( trim( $metaType ) );

	if( !in_array( $metaType, array( "TEXT", "LIST" ) ) )
		$metaType = "TEXT";

	$userMeta = getUserMeta( $meta, $userId );

	# Inserir dado genérico, caso não exista.
	if( is_null( $userMeta ) ) {
		# Tratamento para listas.
		if( $metaType === "LIST" )
			$value = json_encode( $value );

		$uMeta = new UserMeta();
		$uMeta->setMetaSlug( $meta );
		$uMeta->setMetaContent( $value );
		$uMeta->setMetaType( $metaType );
		$uMeta->setMetaUserID( $userId );
		$uMeta->post();
	}
	# Atualizar dado genérico existente.
	else{
		# Tratamento para listas.
		if( $metaType === "LIST" )
			$value = json_encode( array_merge( (array) $userMeta, (array) $value ) );

		$uMeta = new UserMeta();
		$uMeta->setMetaContent( $value );
		$uMeta->setMetaUserId( $userId );
		$uMeta->setMetaSlug( $meta );
		$uMeta->put();
	}
}

/**
 * Obter sessões ativas do usuário.
 * @return array
 */
function getUserSessions() {
	global $userSessions;
	return $userSessions;
}

/**
 * Iniciar navegação principal externa.
 */
function initNavigation() {

}

/**
 * Iniciar navegação principal do setup.
 */
function initSetupNavigation() {
	# Menu principal do painel de administração: itens básicos.
	$setupMenu = new appNavigator( "setup", getSetting( "system_name" ), "setup", true );
	$setupFooterMenu = new appNavigator( "setup-footer", getSetting( "system_name" ), "setup", false, 1 );

	$setupMenu->addItem( array(
		"id"		=> "dashboard",
		"title"		=> gettext( "Dashboard" ),
		"icon"		=> "home",
		"default"	=> true
	) );

	$setupMenu->addItem( array(
		"id"	=> "feed",
		"title"	=> gettext( "Feed" ),
		"icon"	=> "clock"
	) );

	$setupMenu->addItem( array(
		"id"	=> "messages",
		"title"	=> gettext( "Messages" ),
		"icon"	=> "comments"
	) );

	# Menu com dados da empresa.
	$setupMenu->addItem( array(
		"id"	=> "company",
		"title"	=> gettext( "Company" ),
		"icon"	=> "building"
	) );

	$setupMenu->addItem( array(
		"id"		=> "people",
		"title"		=> gettext( "People" ),
		"parent"	=> "company"
	) );

	$setupMenu->addItem( array(
		"id"		=> "employees",
		"title"		=> gettext( "Employees" ),
		"parent"	=> "company"
	) );

	$setupMenu->addItem( array(
		"id"		=> "organization",
		"title"		=> gettext( "Organization" ),
		"parent"	=> "company"
	) );

	$setupMenu->addItem( array(
		"id"	=> "training",
		"title"	=> gettext( "Training" ),
		"icon"	=> "graduation-cap"
	) );

	$setupMenu->addItem( array(
		"id"	=> "events",
		"title"	=> gettext( "Events" ),
		"icon"	=> "calendar"
	) );

	$setupMenu->addItem( array(
		"id"	=> "reports",
		"title"	=> gettext( "Reports" ),
		"icon"	=> "file-export"
	) );

	$setupMenu->addItem( array(
		"id"	=> "utilities",
		"title"	=> gettext( "Utilities" ),
		"icon"	=> "tools"
	) );

	$setupMenu->addItem( array(
		"id"	=> "drive",
		"title"	=> gettext( "Drive" ),
		"icon"	=> "hdd"
	) );

	$setupMenu->addItem( array(
		"id"	=> "wiki",
		"title"	=> gettext( "Wiki" ),
		"icon"	=> "book"
	) );

	$setupMenu->addItem( array(
		"id"	=> "data",
		"title"	=> gettext( "Data" ),
		"icon"	=> "database",
		"role"	=> "owner"
	) );

	# Menu de configurações.
	$setupMenu->addItem( array(
		"id"	=> "settings",
		"title"	=> gettext( "Settings" ),
		"icon"	=> "cog"
	) );

	$setupMenu->addItem( array(
		"id"		=> "general",
		"title"		=> gettext( "General" ),
		"parent"	=> "settings",
		"role"		=> "admin"
	) );

	$setupMenu->addItem( array(
		"id"		=> "security",
		"title"		=> gettext( "Security" ),
		"parent"	=> "settings",
		"admin"		=> "admin"
	) );

	$setupMenu->addItem( array(
		"id"		=> "users",
		"title"		=> gettext( "Users" ),
		"parent"	=> "settings",
		"role"		=> "admin"
	) );

	$setupMenu->addItem( array(
		"id"		=> "authentication",
		"title"		=> gettext( "Authentication" ),
		"parent"	=> "settings",
		"role"		=> "owner"
	) );

	$setupMenu->addItem( array(
		"id"		=> "account",
		"title"		=> gettext( "Account" ),
		"parent"	=> "settings"
	) );

	$setupMenu->addItem( array(
		"id"		=> "profile",
		"title"		=> gettext( "Profile" ),
		"page"		=> "account",
		"parent"	=> "account"
	) );

	$setupMenu->addItem( array(
		"id"		=> "user-contacts",
		"title"		=> gettext( "User contacts" ),
		"page"		=> "account",
		"parent"	=> "account"
	) );

	$setupMenu->addItem( array(
		"id"		=> "account-security",
		"title"		=> gettext( "Security and login" ),
		"page"		=> "account",
		"parent"	=> "account"
	) );

	$setupMenu->addItem( array(
		"id"		=> "privacy",
		"title"		=> gettext( "Privacy" ),
		"page"		=> "account",
		"parent"	=> "account"
	) );

	$setupMenu->addItem( array(
		"id"		=> "notification",
		"title"		=> gettext( "Notifications" ),
		"page"		=> "account",
		"parent"	=> "account"
	) );

	$setupMenu->addItem( array(
		"id"		=> "integration",
		"title"		=> gettext( "Integration" ),
		"parent"	=> "settings",
		"role"		=> "admin"
	) );

	$setupMenu->addItem( array(
		"id"	=> "god-mode",
		"title"	=> gettext( "God mode" ),
		"icon"	=> "globe-americas",
		"role"	=> "god-mode"
	) );

	$setupMenu->addItem( array(
		"id"		=> "entity-catalog",
		"title"		=> gettext( "Entity catalog" ),
		"parent"	=> "god-mode",
		"page"		=> "catalog"
	) );

	# Itens ocultos
	$setupMenu->addItem( array(
		"id"	=> "notifications",
		"title"	=> gettext( "Notifications" ),
		"hide"	=> true
	) );

	$setupMenu->addItem( array(
		"id"	=> "favorites",
		"title"	=> gettext( "Favorites" ),
		"hide"	=> true
	) );


	# Menu inferior básico.
	$setupFooterMenu->addItem( array(
		"id"		=> "terms",
		"slug"		=> "help",
		"params"	=> array( "view" => "terms" ),
		"title"		=> gettext( "Terms of service" )
	) );

	$setupFooterMenu->addItem( array(
		"id"		=> "privacy",
		"slug"		=> "help",
		"params"	=> array( "view" => "privacy" ),
		"title"		=> gettext( "Privacy policy" )
	) );

	$setupFooterMenu->addItem( array(
		"id"		=> "cookies",
		"slug"		=> "help",
		"params"	=> array( "view" => "cookies" ),
		"title"		=> gettext( "Cookies policy" )
	) );

	$setupFooterMenu->addItem( array(
		"id"	=> "help",
		"title"	=> gettext( "Help" )
	) );

	$setupFooterMenu->addItem( array(
		"id"	=> "feedback",
		"title"	=> gettext( "Feedback" )
	) );

	$setupFooterMenu->addItem( array(
		"id"	=> "language",
		"slug"	=> "settings",
		"title"	=> '<i class="fas fa-language"></i>&nbsp;&nbsp;' . gettext( getAvailableLanguages( getSetting( "language" ) ) ) . '&nbsp;&nbsp;<i class="fas fa-caret-right"></i>'
	) );

	$setupFooterMenu->addItem( array(
		"id"		=> "report",
		"slug"		=> "feedback",
		"params"	=> array( "category" => "problem" ),
		"title"		=> gettext( "Feedback" ),
		"hide"		=> true
	) );
}

/**
 * Obter menu através do ID.
 * @param  string $id   ID único do menu.
 * @param  string $echo Definir se a estrutura do menu deve ser impressa.
 * @return object
 */
function getMenu( $id = "", $echo = true ) {
	if( empty( $id ) )
		$id = "main-menu";

	$menu = appNavigator::getMenuObj( $id );

	if( $echo )
		echo $menu->getMenu( $id );
	
	return $menu;
}

/**
 * Obter URL de item do menu a partir da sua identificação.
 * @param  string $menuId ID único do menu.
 * @param  string $itemId ID único do item do menu.
 * @return string
 */
function getMenuURL( $menuId, $itemId ) {
	$menu = getMenu( $menuId, false );
	return $menu->getURL( $itemId );
}

/**
 * Obter versão do sistema a partir do GIT.
 * @param  boolean $numeric Definir se o formato da versão deve ser numérico.
 * @return string
 */
function getVersion( $numeric = true ) {
	return easyForm::getVersion();
}

/**
 * Verificar se a página está marcada como favorita.
 * @param  string  $id   ID único do item do menu.
 * @param  string  $menu ID único do menu.
 * @return boolean
 */
function isFavorite( $id, $menu ) {
	$favorite = new Favorite();
	$favorite->get( array(
		array(
			"key"	=> "favoriteSlug",
			"value"	=> $id
		),
		array(
			"key"	=> "favoriteMenu",
			"value"	=> $menu
		),
		array(
			"key"	=> "favoriteUserId",
			"value"	=> getUserID()
		) 
	) );

	if( !is_null( $favorite->getFavoriteID() ) )
		return true;

	return false;
}

/**
 * Obter lista de pasta de favoritos em cascata.
 * @return array
 */
function getFavoriteFolderCascade() {
	# Obter todas as pastas de favoritos do usuário.
	$ffolder = new FavoriteFolder();
	$hasFFolder = $ffolder->get( array(
		array(
			"key"	=> "ffolderUserId",
			"value"	=> getUserID()
		)
	) );

	$folders = array();

	if( $hasFFolder ) {
		do {
			$folders[] = $ffolder->getFields();
		}while( $ffolder->next() );
	}

	# Adicionar níveis para a lista.
	return addListLevelsByAttr( $folders, "ffolderid", "ffolderparentid" );;
}

/**
 * Adicionar atributo de nível para listas simples.
 * @param string  $list      Lista para tratamento.
 * @param string  $key       Atributo que contém a principal da lista.
 * @param string  $parentKey Atributo que contém a chave estrangeira da lista.
 * @param string  $value     Valor inicial de comparação, geralmente vazio.
 * @param integer $level     Nível inicial da lista, geralmente iniciado por zero.
 * @return array
 */
function addListLevelsByAttr( $list, $key, $parentKey, $value = "", $level = 0 ) {
	$array = array();

	foreach( $list as $item ) {
		if( trim( $item[ ( $parentKey ) ] ) === $value ) {
			$item[ "level" ] = $level + 1;
			$array[] = $item;
			$array = array_merge( $array, addListLevelsByAttr( $list, $key, $parentKey, $item[ ( $key ) ], $item[ "level" ] ) );
		}
	}

	return $array;
}