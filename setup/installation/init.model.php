<?php
/**
 * Configurações de inicialização do Fusy Framework.
 * Arquivo gerado automaticamente.
 */

# Configurações de conexão
define( "DB_MANAGER", "{%DB_MANAGER%}" );
define( "DB_HOST", "{%DB_HOST%}" );
define( "DB_PORT", "{%DB_PORT%}" );
define( "DB_USER", "{%DB_USER%}" );
define( "DB_PASSWORD", "{%DB_PASSWORD%}" );
define( "DB_NAME", "{%DB_NAME%}" );
define( "DB_SID", "{%DB_SID%}" );

# Definição do diretório de tabelas do projeto.
smoothConnect::sourcePath( "./setup/src/tables/" );

# Definir idioma e fuso horário do sistema.
if( $linguage = getSystemLanguage() )
	locale_set_default( $linguage );

if( $timezone = getSystemTimezone() )
	date_default_timezone_set( $timezone );

# Inicializar API.
if( $apiRequest = apiRequest() ) {
	new smoothRequest( $apiRequest );
	exit;
}

# Definir domínio de texto padrão.
set_textdomain( "fusy", "./languages/" );