<?php
/**
 * Formulário para configuração do usuário administrador do sistema.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

global $installSetts;
$form = new easyForm();

$form->addSection( array(
	"title"			=> gettext( "Configure administrator user" ),
	"description"	=> gettext( "Define the access credentials of the primary user of the system." ),
	"cols"			=> 1
) );

$form->addField( getObjectField( "person", "fullname", array(
	"label"		=> gettext( "Name" ),
	"value"		=> gettext( "Administrator" ),
	"legend"	=> gettext( "Inform the name of the system administrator." )
) ) );

$form->addField( getObjectField( "email", "emailAddress", array(
	"label"		=> gettext( "E-mail" ),
	"legend"	=> gettext( "Inform the main e-mail of the system administrator. This user will have full control of the system settings and this email can be changed later." )
) ) );

$form->addField( getObjectField( "user", "username", array(
	"label"		=> gettext( "Username" ),
	"value"		=> "admin",
	"legend"	=> gettext( "Inform the username of the system administrator. This username will be one of the access credentials, along with the primary email." )
) ) );

$form->addField( getObjectField( "person", "birthday", array(
	"label"		=> gettext( "Birthday" )
) ) );

$form->addField( getObjectField( "user", "password", array(
	"label"		=> gettext( "Password" ),
	"legend"	=> gettext( "Inform the system administrator password." )
) ) );

$form->addField( getObjectField( "user", "password", array(
	"name"		=> "confirmPassword",
	"label"		=> gettext( "Confirm password" ),
	"clone"		=> "password",
	"legend"	=> gettext( "Confirm the system administrator password." )
) ) );

# Definir valor dos campos.
if( isset( $installSetts[ "account" ] ) ) {
	try{
		$account = json_decode( $installSetts[ "account" ], true );
	}catch( Exception $e ) {
		$account = array();
	}

	foreach( $account as $name => $value ) {
		if( !is_array( $value ) )
			$form->setFieldValue( $name, $value );
	}
}

$form->onSuccess( function( $response, $form ) {
	$config = array();

	# Listar campos para salvar como JSON.
	foreach( $response as $id => $r ) {
		if( $id != "confirmPassword" )
			$config[ ( $id ) ] = $r[ "db_value" ];
	}

	$config = json_encode( $config );

	# Inserir ou atualizar configuração.
	$setting = new Setting();
	$setting->setSlug( "account" );
	$setting->setContent( $config );

	if( !$setting->post( true ) ) {
		$form->setMessage( easyNotification::ERROR, gettext( "The settings could not be saved. Please check the data provided and try again." ), null, $setting->getError() );
	}else{
		# Atualizar etapa de instalação.
		$setting = new Setting();
		$setting->setSlug( "step" );
		$setting->setContent( "application" );
		$setting->post( true );

		$form->setSettings( array( "redirect" => getPageURL() ) );
	}
});

$form->getForm();