<?php
/**
 * Formulário para definição de linguagem padrão do framework.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$form = new easyForm();
$form->addSection( array(
	"title"		=> gettext( "System language and time zone" ),
	"cols"		=> 1
) );

$form->addField( array(
	"name"		=> "language",
	"id"		=> "language",
	"label"		=> gettext( "Language" ),
	"required"	=> true,
	"type"		=> "select",
	"options"	=> getAvailableLanguages(),
	"value"		=> locale_get_default()
) );

$form->addField( array(
	"name"		=> "timezone",
	"id"		=> "timezone",
	"label"		=> gettext( "Timezone" ),
	"required"	=> true,
	"type"		=> "timezone"
) );

$form->onSuccess( function( $response, $form ) {
	# Salvar campos na sessão.
	setSystemLanguage( $response[ "language" ][ "db_value" ] );
	setSystemTimezone( $response[ "timezone" ][ "db_value" ] );

	$form->setSettings( array( "redirect" => getPageURL() ) );
});

$form->getForm();