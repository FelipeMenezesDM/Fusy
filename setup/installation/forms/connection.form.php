<?php
/**
 * Formulário para criação de arquivo de conexão com a base de dados.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

$form = new EasyForm( array(
	"submit"	=> gettext( "Connect" )
) );

$form->addSection( array(
	"title"		=> gettext( "Configure connection to the database" ),
	"cols"		=> 1
) );

$form->addField( array(
	"name"		=> "sgbd",
	"id"		=> "sgbd",
	"label"		=> gettext( "Manager" ),
	"required"	=> true,
	"type"		=> "select",
	"sensitive"	=> false,
	"options"	=> array(
		array( "value" => "", "title" => gettext( "Select manager" ) ), 
		array( "value" => "sqlserver", "title" => "SQL Server" ), 
		array( "value" => "mysql", "title" => "MySQL" ), 
		array( "value" => "postgres", "title" => "PostgreSQL" ), 
		array( "value" => "oracle", "title" => "Oracle" )
	),
	"value"		=> "mysql",
	"legend"	=> gettext( "Select a manager compatible with the framework from the selection list above." )
) );

$form->addField( array(
	"name"		=> "host_name",
	"id"		=> "host_name",
	"label"		=> gettext( "Server" ),
	"value"		=> "localhost",
	"legend"	=> gettext( "Enter the name or IP of the database server." ),
	"required"	=> true
) );

$form->addField( array(
	"name"		=> "host_port",
	"id"		=> "host_port",
	"label"		=> gettext( "Port" ),
	"legend"	=> gettext( "It is not necessary to inform if you are using the default manager port." ),
	"mask"		=> "9#"
) );

$form->addField( array(
	"name"		=> "db_sid",
	"id"		=> "db_sid",
	"label"		=> "SID",
	"legend"	=> gettext( "For Oracle connections, it is necessary to inform the SID of the database." ),
	"required"	=> true,
	"dependency" => array(
		"field"		=> "sgbd",
		"value"		=> "oracle",
		"compare"	=> "="
	)
) );

$form->addField( array(
	"name"		=> "db_name",
	"id"		=> "db_name",
	"label"		=> gettext( "Database" ),
	"legend"	=> gettext( "Enter the name of the main database." ),
	"required"	=> true
) );

$form->addField( array(
	"name"		=> "db_user",
	"label"		=> gettext( "Username" ),
	"legend"	=> gettext( "Inform a user who has permissions to create bases, schemas and to consult table and column metadata. This is necessary for the application to work." ),
	"required"	=> true
) );

$form->addField( array(
	"name"		=> "db_pass",
	"id"		=> "db_pass",
	"label"		=> gettext( "Password" ),
	"type"		=> "password",
	"encrypt"	=> "defaultDataEncrypt",
	"validate"	=> false
) );

$form->onSuccess( function( $response, $form ) {
	$manager = $response[ "sgbd" ][ "db_value" ];
	$database = $response[ "db_name" ][ "db_value" ];
	$user = $response[ "db_user" ][ "db_value" ];
	$password = defaultDataDecrypt( $response[ "db_pass" ][ "db_value" ] );
	$host = $response[ "host_name" ][ "db_value" ];
	$port = $response[ "host_port" ][ "db_value" ];
	$sid = $response[ "db_sid" ][ "db_value" ];

	$conn = new smoothConnect( $database, $user, $password, $host, $port, $sid );
	$conn->setManager( $manager );
	
	# Testar conexão.
	try{
		$conn->connect();
	}catch( Exception $e ){}

	# Verificar status da conexão.
	if( !$conn->getConnectionStatus() ) {
		$form->setMessage( easyNotification::ERROR, gettext( "Could not connect to the database. Please check the data provided and try again." ), null, $conn->getError() );
	}else{
		# Criar arquivo de cnfiguração a partir do modelo.
		$init = _file_get_contents( "./setup/installation/init.model.php" );
		$init = str_replace(
			array( "{%DB_MANAGER%}", "{%DB_HOST%}", "{%DB_PORT%}", "{%DB_USER%}", "{%DB_PASSWORD%}", "{%DB_NAME%}", "{%DB_SID%}" ),
			array( $manager, $host, $port, $user, $password, $database, $sid ),
			$init
		);

		$file = _fopen( "./setup/init.php", "w" );
		fwrite( $file, $init );
		fclose( $file );

		# Incluir arquivo de inicialização criado.
		getSetupPage( "init" );

		# Criação de tabelas na base de dados.
		smoothModeler::processTables();

		# Redirecionar formulário.
		$form->setSettings( array( "redirect" => getPageURL() ) );
	}
});

$form->getForm();