<?php
/**
 * Formulário para configuração de informações adicionais do sistema.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

global $installSetts;
$form = new easyForm();

$form->addSection( array(
	"title"			=> gettext( "Additional system settings" ),
	"description"	=> gettext( "Global settings for the system, such as primary e-mail server and system identity." ),
	"cols"			=> 1,
	"id"			=> "identity"
) );

$form->addField( array(
	"name"		=> "system_name",
	"id"		=> "system_name",
	"required"	=> true,
	"label"		=> gettext( "System name" ),
	"legend"	=> gettext( "This name will be used to identify the system in general." ),
	"value"		=> "Fusy Framework",
	"section"	=> "identity"
) );

$form->addField( array(
	"name"		=> "system_abbr",
	"id"		=> "system_abbr",
	"value"		=> "FUSY",
	"type"		=> "pattern",
	"regex"		=> "/^[a-z0-9]*$/i",
	"required"	=> true,
	"label"		=> gettext( "System abbreviation" ),
	"legend"	=> gettext( "Unique system identifier, which will be used for internal routines. It must not contain special characters or blanks." ),
	"section"	=> "identity",
	"transform"	=> "upper"
) );

$form->addField( array(
	"name"		=> "home_page",
	"id"		=> "home_page",
	"required"	=> true,
	"label"		=> gettext( "Home page" ),
	"type"		=> "selectlist",
	"options"	=> array(
		array( "value" => "default_home_page", "title" => gettext( "Default home page" ) ),
		array( "value" => "login", "title" => gettext( "Login page" ) )
	),
	"value"		=> "login",
	"legend"	=> gettext( "Define the default home page for the system." ),
	"section"	=> "identity"
) );

$form->addField( array(
	"name"		=> "allow_user_registration",
	"id"		=> "allow_user_registration",
	"required"	=> true,
	"label"		=> gettext( "User registration" ),
	"type"		=> "selectlist",
	"options"	=> array(
		array( "value" => "yes", "title" => gettext( "Allow users to register on the system" ) ),
		array( "value" => "no", "title" => gettext( "Do not allow users to register on the system" ) )
	),
	"value"		=> "no",
	"legend"	=> gettext( "Define whether users will have autonomy to register to access the system." ),
	"section"	=> "identity"
) );

$form->addField( array(
	"name"		=> "default_user_role",
	"id"		=> "default_user_role",
	"required"	=> true,
	"type"		=> "selectlist",
	"label"		=> gettext( "Standard attribution" ),
	"legend"	=> gettext( "Set the default assignment for new system users." ),
	"options"	=> array(
		array( "value" => "guest", "title" => gettext( "Guest" ) ),
		array( "value" => "owner", "title" => gettext( "Owner" ) ),
		array( "value" => "administrator", "title" => gettext( "Administrator" ) )
	),
	"value"		=> "guest",
	"section"	=> "identity",
	"dependency" => array(
		"field"		=> "allow_user_registration",
		"value"		=> "yes",
		"compare"	=> "="
	)
) );

$form->addField( array(
	"name"		=> "allow_user_multiple_session",
	"id"		=> "allow_user_multiple_session",
	"required"	=> true,
	"label"		=> gettext( "User session" ),
	"type"		=> "selectlist",
	"options"	=> array(
		array( "value" => "yes", "title" => gettext( "Allow multiple user sessions" ) ),
		array( "value" => "no", "title" => gettext( "Do not allow multiple user sessions" ) )
	),
	"value"		=> "no",
	"legend"	=> gettext( "Define whether users can create simultaneous sessions with different accounts on the system." ),
	"section"	=> "identity"
) );

$form->addSection( array(
	"title"			=> gettext( "Email server" ),
	"description"	=> gettext( "Set the credentials of the e-mail server for sending internal system messages." ),
	"cols"			=> 1,
	"id"			=> "email"
) );

$form->addField( array(
	"name"		=> "email_server_host",
	"id"		=> "email_server_host",
	"label"		=> gettext( "Server" ),
	"legend"	=> gettext( "SMTP server address." ),
	"section"	=> "email"
) );

$form->addField( array(
	"name"		=> "email_server_host_port",
	"id"		=> "email_server_host_port",
	"mask"		=> "9#",
	"label"		=> gettext( "Port" ),
	"legend"	=> gettext( "SMTP server port. If not, the default SMTP port will be used." ),
	"section"	=> "email",
	"dependency" => array(
		"field"		=> "email_server_host",
		"value"		=> "",
		"compare"	=> "!="
	)
) );

$form->addField( array(
	"name"		=> "email_server_certificate",
	"id"		=> "email_server_certificate",
	"label"		=> gettext( "Certificate" ),
	"options"	=> array(
		array( "value" => "tls", "title" => "TLS" ),
		array( "value" => "ssl", "title" => "SSL" )
	),
	"value"		=> "tls",
	"type"		=> "selectlist",
	"section"	=> "email",
	"required"	=> true,
	"dependency" => array(
		"field"		=> "email_server_host",
		"value"		=> "",
		"compare"	=> "!="
	)
) );

$form->addField( array(
	"name"		=> "email_server_username",
	"id"		=> "email_server_username",
	"label"		=> gettext( "Username" ),
	"legend"	=> gettext( "Standard user for the SMTP server." ),
	"required"	=> true,
	"type"		=> "email",
	"section"	=> "email",
	"dependency" => array(
		"field"		=> "email_server_host",
		"value"		=> "",
		"compare"	=> "!="
	)
) );

$form->addField( array(
	"name"		=> "email_server_password",
	"id"		=> "email_server_password",
	"label"		=> gettext( "Password" ),
	"legend"	=> gettext( "Default user password for the SMTP server." ),
	"required"	=> true,
	"section"	=> "email",
	"type"		=> "password",
	"validate"	=> false,
	"encrypt"	=> "defaultDataEncrypt",
	"dependency" => array(
		"field"		=> "email_server_host",
		"value"		=> "",
		"compare"	=> "!="
	)
) );

# Definir valor dos campos.
if( isset( $installSetts[ "application" ] ) ) {
	try{
		$application = json_decode( $installSetts[ "application" ], true );
	}catch( Exception $e ) {
		$application = array();
	}

	foreach( $application as $name => $value ) {
		if( !is_array( $value ) )
			$form->setFieldValue( $name, $value );
	}
}

$form->onSuccess( function( $response, $form ) {
	$config = array();

	# Listar campos para salvar como JSON.
	foreach( $response as $id => $r )
		$config[ ( $id ) ] = $r[ "db_value" ];

	$config = json_encode( $config );

	# Inserir ou atualizar configuração.
	$setting = new Setting();
	$setting->setSlug( "application" );
	$setting->setContent( $config );

	if( !$setting->post( true ) ) {
		$form->setMessage( easyNotification::ERROR, gettext( "The settings could not be saved. Please check the data provided and try again." ), null, $setting->getError() );
	}else{
		# Atualizar etapa de instalação.
		$setting = new Setting();
		$setting->setSlug( "step" );
		$setting->setContent( "confirmation" );
		$setting->post( true );

		$form->setSettings( array( "redirect" => getPageURL() ) );
	}
});

$form->getForm();