<?php
/**
 * Formulário para confirmação dos termos de uso do framework.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

global $installSetts;
$form = new easyForm( array( "layout" => "form", "submit" => gettext( "Install" ) ) );

$form->addSection( array(
	"title"		=> gettext( "Terms of use and distribution" ),
	"cols"		=> 1,
	"before"	=> '<div id="termsWrap"></div>'
) );

$form->addField( array(
	"name"		=> "confirm",
	"id"		=> "confirm",
	"type"		=> "checkbox",
	"title"		=> gettext( "Accept terms" ),
	"onSuccess"	=> function() {
		global $field;

		if( empty( $field[ "value" ] ) )
			$field[ "error" ] = gettext( "You must accept the terms to confirm the installation of the framework." );
	}
) );

$form->onSuccess( function( $response, $form ) {
	global $installSetts;

	# Processar estrutura da base de dados.
	try{ $application = json_decode( $installSetts[ "application" ], true ); }
	catch( Exception $e ) { $application = array(); }

	try{ $config = json_decode( $installSetts[ "account" ], true ); }
	catch( Exception $e ) { $config = array(); }

	$personHash = uuid();
	$person = new Person();
	$person->setFullname( $config[ "fullname" ] );
	$person->setPersonHash( $personHash );
	$person->setBirthday( $config[ "birthday" ] );
	$person->setType( "F" );

	# Cadastrar pessoa no sistema.
	if( !$person->post() ) {
		$form->setMessage( easyNotification::ERROR, gettext( "It was not possible to register the user in the system. Please check the data provided and try again." ), null, $person->getError() );
		return;
	}

	# Obter pessoa a partir de seu hash único.
	$person->getByHash( $personHash );

	$email = new Email();
	$email->setEmailAddress( $config[ "emailaddress" ] );
	$email->setEmailIsPrimary( 1 );
	$email->setEmailPersonId( $person->getPersonId() );

	# Cadastrar endereço de e-mail no sistema.
	if( !$email->post() ) {
		$person->delete();
		$form->setMessage( easyNotification::ERROR, gettext( "It was not possible to register the user in the system. Please check the data provided and try again." ), null, $email->getError() );
		return;
	}

	$user = new User();
	$user->setUsername( $config[ "username" ] );
	$user->setPassword( $config[ "password" ] );
	$user->setUserPersonId( $person->getPersonId() );
	
	# Castrar usuário no sistema.
	if( !$user->post() ) {
		$email->delete();
		$person->delete();
		$form->setMessage( easyNotification::ERROR, gettext( "It was not possible to register the user in the system. Please check the data provided and try again." ), null, $user->getError() );
		return;
	}

	$user->getByUsername( $config[ "username" ] );

	# Salvar idioma selecionado.
	if( getSystemLanguage() )
		$application[ "language" ] = getSystemLanguage();

	# Salvar fuso horário selecionado.
	if( getSystemTimezone() )
		$application[ "timezone" ] = getSystemTimezone();

	# Contagem de errors.
	$errorCount = 0;

	# Salvar configurações.
	foreach( $application as $slug => $content ) {
		$setting = new Setting();
		$setting->setSlug( $slug );
		$setting->setContent( ( is_null( $content ) ? "" : $content ) );

		if( !$setting->post( true ) )
			$errorCount++;
	}

	if( $errorCount > 0 ) {
		$email->delete();
		$user->delete();
		$person->delete();
		$form->setMessage( easyNotification::ERROR, gettext( "The Framework installation could not be completed. Please check if the server is available and try again." ) );
	}else{
		# Remover parâmetros de instalação do sistema.
		Setting::_delete( array( "meta_query" => array( array( "key" => "slug", "compare" => "IN", "value" => array( "application", "account", "step" ) ) ) ) );

		# Definir cor padrão do usuário.
		putUserMeta( "color", letterVector::getRandColor(), "TEXT", $user->getUserID() );

		# Atualizar status do sistema.
		if( !defined( "SYSTEM_STATUS" ) ) {
			$init = _file_get_contents( "./setup/init.php" ) . "\n\n# Definir status do sistema.\ndefine( \"SYSTEM_STATUS\", true );";
			$file = _fopen( "./setup/init.php", "w" );
			fwrite( $file, $init );
			fclose( $file );
		}

		# Atraso para redirecionamento.
		sleep(2);

		$form->setSettings( array( "redirect" => addQueryArgs( array( "user" => "signin" ), getAppURL() ) ) );
	}
});

$form->getForm();