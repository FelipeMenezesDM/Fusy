<?php
/**
 * Formulário de instalação do Fusy Framework.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title><?php echo gettext( "Fusy Framework installation" ); ?></title>
	<?php getStyles( "installation" ); # Carregamento de estilos. ?>
</head>

<body>
	<div class="wrapper">
		<div class="install-body">
			<div class="install-header">
				<h2 class="install-title"><?php echo gettext( "Welcome to the Fusy Framework installation" ); ?></h2>
				<p class="install-legend"><?php echo gettext( "Provide the information required for installation below. Remembering that this information can be changed later." ); ?></p>
			</div>
		<?php

			if( !getSystemLanguage() ) {
				_require( "./setup/installation/forms/locale.form.php" );
			}elseif( !_file_exists( "./setup/init.php" ) ) {
				_require( "./setup/installation/forms/connection.form.php" );
			}else{
				global $installSetts;

				$step = "account";
				$installSetts = array();

				# Obter configurações de instalação.
				$settings = new Setting();
				$settings->get();

				# Listar configurações de instalação.
				do{
					$installSetts[ ( $settings->getSlug() ) ] = $settings->getContent();
				}while( $settings->next() );

				# Definir etapa da instalação.
				if( isset( $installSetts[ "step" ] ) )
					$step = $installSetts[ "step" ];

				_require( "./setup/installation/forms/${step}.form.php" );
			}

		?>
		</div>
	</div>
	<?php getScripts( "easyform" ); # Carregamento de scripts ?>
</body>