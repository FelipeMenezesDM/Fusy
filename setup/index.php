<?php
/**
 * Arquivo de contralização de páginas para o painel de administração.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

# Redirecionar para a página de login, caso a sessão do usuário não exista.
checkAuthAccess();

# Obter menu principal do setup.
global $setupMenu, $currentSetupMenuItem;
$backwardURL = $dashboardURL = getMenuURL( "setup", $setupMenu->getDefaultItemID() );

# Obter URL de retono para páginas de erro, caso exista.
if( isAjax() && isset( $_REQUEST[ "backwardURL" ] ) && !empty( trim( $_REQUEST[ "backwardURL" ] ) ) )
	$backwardURL = trim( $_REQUEST[ "backwardURL" ] );

global $currentUser;
$error = false;

# Página não encontrada.
if( !$currentSetupMenuItem ) {
	$error = array(
		"code"	=> "404",
		"title"	=> gettext( "Page not found" ),
		"msg"	=> gettext( "Sorry, but the link you followed may be broken or removed." )
	);
}
# Acesso negado.
elseif( !getUserRole( $currentSetupMenuItem[ "role" ] ) ) {
	$error = array(
		"code"	=> "403",
		"title"	=> gettext( "Access denied" ),
		"msg"	=> gettext( "You do not have sufficient access permissions to access this page." )
	);
}
# Sessão expirada.
elseif( is_null( $currentUser ) ) {
	$error = array(
		"code"	=> "401",
		"title"	=> gettext( "Session expired" ),
		"msg"	=> gettext( "To access this page you need to be in an authenticated session." )
	);
}

# ID padrão do item do menu.
$uniqid = uniqid();
$menuID = $itemID = $uniqid;

if( $error ) {
	# Redefinir título da página.
	setPageTitle( $error[ "title" ] . " | " . getSetting( "system_name" ) );
	$title = $error[ "title" ];
}else{
	setPageTitle( $currentSetupMenuItem[ "page_title" ] );
	$title = $currentSetupMenuItem[ "title" ];
	$menuID = $setupMenu->getID() . "-" . ( count( $currentSetupMenuItem[ "parents" ] ) >= 2 ? $currentSetupMenuItem[ "parents" ][1] : $currentSetupMenuItem[ "slug" ] );
	$itemID = $setupMenu->getID() . "-" . $currentSetupMenuItem[ "slug" ];
}

# Incluir cabeçalho quando requisição não for Ajax.
if( isAjax() || isPrinting() ) :
	ob_start();
else :
	getHeader( "setup" );
?>
<div class="setup-body">
	<div class="mini-tabs-box ef-table-height-line">
		<div class="mini-tabs-wrapper">
			<div class="mini-tabs-container">
				<div class="mini-tabs-itens">
					<div
						class="mini-tab selected<?php echo ( $currentSetupMenuItem[ "default" ] ? " is-default fixed-tab" : "" ); ?>"
						title="<?php echo $title . ( !isAjax() ? ( '" data-tab-title="' . getPageTitle() . '" data-tab-url="' . getPageURL() ) : "" ); ?>"
						data-item-id="<?php echo $itemID; ?>" data-tab-id="<?php echo $menuID; ?>"
					>
						<h4><span><?php echo $title; ?></span></h4>
						<a href="<?php echo $dashboardURL; ?>" data-tab-ignore="true" class="close-act"><i class="fas fa-times"></i></a>
					</div>
				</div>
			</div>
		</div>
		<div class="mini-tabs-nav">
			<button class="mini-tabs-nav-act prev"><i class="fas fa-angle-left"></i></button>
			<button class="mini-tabs-nav-act next"><i class="fas fa-angle-right"></i></button>
			<div class="mini-tabs-simple-nav drop-down-container">
				<button class="mini-tabs-nav-act drop-down-handler"><i class="fas fa-angle-down"></i></button>
				<div class="mini-tabs-nav-links drop-down-menu floating-box">
					<ul class="default-link-menu"></ul>
				</div>
			</div>
		</div>
	</div>

	<div class="mini-tabs-pages">
<?php endif; ?>
		<div class="mini-tab-page page-content pg-mn-<?php echo $menuID; ?>" data-title="<?php echo $title; ?>" data-item-id="<?php echo $itemID; ?>" data-tab-id="<?php echo $menuID; ?>">
			<div class="easyform-preloader tab-preloader">
				<a href="#" class="easyform-field type-button cancel-request" data-tab-ignore="true"><?php echo gettext( "Cancel" ); ?></a>
			</div>
		<?php
			if( !$error ) :
				# Obter migalha da aba.
				$crumb = "";
				$crumbItens = $currentSetupMenuItem[ "parents" ];
				$crumbItens[] = $currentSetupMenuItem[ "id" ];

				# Listagem de itens do menu para a migalha.
				foreach( $crumbItens as $itemId ) {
					$item = $setupMenu->getItem( $itemId );
					$isCurrent = ( strtolower( $currentSetupMenuItem[ "id" ] ) === strtolower( $item[ "id" ] ) );
					$linkedMenu = '<span class="crumb-nav-btn drop-down-handler"><i class="fas fa-caret-square-down"></i></span>' .
								  '<div class="floating-box drop-down-menu">' .
										$setupMenu->getAlignedMenu( $item[ "id" ] ) .
								  '</div>';
					$crumb .= '<li class="' . ( $isCurrent ? 'current' : 'parent' ) . ' drop-down-container drop-down-autoclose">' .
								( $isCurrent ? $item[ "title" ] : '<a href="' . $item[ "url" ] . '" title="' . $item[ "title" ] . '">' . $item[ "title" ] . '</a>' ) .
								$linkedMenu . ( !$isCurrent ? '<span class="arrow">&nbsp;&rsaquo;&nbsp;</span>' : "" ) .
							  '</li>';
				}
		?>
				<div class="page-head ef-table-height-line">
					<div class="page-crumb-title">
						<ul><?php echo $crumb; ?></ul>
					</div>
					<div class="page-actions">
					<?php if( getUserRole( "audition" ) ) : ?>
						<a href="#" class="page-action versions" data-tab-ignore="true"><i class="fas fa-code-branch"></i></a>
					<?php endif; ?>
						<a href="#" class="page-action help" data-tab-ignore="true"><i class="fas fa-question-circle"></i></a>
						<a href="#" class="page-action favorite<?php echo ( isFavorite( $currentSetupMenuItem[ "id" ], $setupMenu->getID() ) ? " selected" : "" ); ?>" data-menu="<?php echo $setupMenu->getID(); ?>" data-id="<?php echo $currentSetupMenuItem[ "id" ]; ?>" data-tab-ignore="true"><i class="fas fa-star"></i></a>
						<a href="<?php echo getPageURL(); ?>" class="page-action refresh"><i class="fas fa-redo"></i></a>
						<a href="<?php echo $dashboardURL; ?>" class="page-action close" data-tab-ignore="true"><i class="fas fa-times"></i></a>
					</div>
					<div class="clearfix"></div>
				</div><!-- .page-head -->
				<div class="page-body">
					<?php getPage( $currentSetupMenuItem[ "page" ] ); ?>
					<div class="clearfix"></div>
				</div>
		<?php else : ?>
			<div class="tab-error-page-container">
				<div class="tab-error-page-wrapper">
					<div class="tab-error-page">
						<h1><?php echo $error[ "code" ]; ?></h1>
						<h2><?php echo $error[ "title" ]; ?></h2>
						<p><?php echo $error[ "msg" ]; ?></p>
						<a href="<?php echo $backwardURL; ?>" class="easyform-field type-button primary"><i class="fas fa-chevron-left"></i> <?php echo gettext( "Back" ); ?></a>
						<a href="<?php echo $dashboardURL; ?>" class="easyform-field type-button page-action close" data-tab-ignore="true"><?php echo gettext( "Close" ); ?></a>
					</div>
				</div>
			</div>
		<?php endif; ?>
		</div>
<?php if( !isAjax() ) : ?>
	</div><!-- .mini-tabs-pages -->
</div><!-- .setup-body -->

<?php
	# Incluir rodapé quando requisição não for Ajax.
	getFooter( "setup" );
endif;