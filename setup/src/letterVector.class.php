<?php
/**
 * Transformar letras em vetores e retornar codificado como base64.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class letterVector {
	/**
	 * Letra para vetorização.
	 * @access private
	 * @var    string
	 */
	private $letter;

	/**
	 * Cor padrão da letra.
	 * @access private
	 * @var    string
	 */
	private $color;

	/**
	 * Tamanho da imagem.
	 * @access private
	 * @var    int
	 */
	private $size;

	/**
	 * Lista de cores para seleção randomica.
	 * @var array
	 */
	private static $colors = array( "#6c8ead", "#f44336", "#e91e63", "#f3e5f5", "#7986cb", "#64b5f6", "#26a69a", "#81c784", "#d4e157", "#ffca28", "#ff7043", "#bdbdbd" );

	/**
	 * Método construtor.
	 * @param string $letter Letra do alfabeto para vetorização.
	 */
	public function __construct( $letter, $color = false, $size = 300 ) {
		if( !preg_match( "/[a-zA-Z]/", $letter ) )
			$letter = "";

		# Selecionar cor aleatoria.
		if( !$color )
			$color =  self::getRandColor();

		$this->letter = strtoupper( $letter );
		$this->color = $color;
		$this->size = $size;
	}

	/**
	 * Obter cor aleatória.
	 * @return string
	 */
	public static function getRandColor() {
		return self::$colors[ rand( 0, count( self::$colors ) - 1 ) ];
	}

	/**
	 * Obter letra vetorizada.
	 * @return string
	 */
	public function getVector() {
		list( $r, $g, $b ) = sscanf( "#" . str_replace( "#", "", $this->color ), "#%02x%02x%02x" );

		$imageSize = $this->size;
		$fontSize = $imageSize * 0.52;
		$imageObj = imagecreate( $imageSize, $imageSize );
		$bgColor = imagecolorallocate( $imageObj, $r, $g, $b );
		$txtColor = imagecolorallocate( $imageObj, $r * 0.6, $g * 0.6, $b * 0.6 );
		$fontFile = __DIR__ . str_replace( array( "/", "\\" ), DIRECTORY_SEPARATOR, "/../../assets/fonts/arial.ttf" );
		$box = imagettfbbox( $fontSize, 0, $fontFile, $this->letter );
		$x = ( ( imagesx( $imageObj ) - abs( max( $box[2], $box[4] ) ) ) / 2 );
		$y = ( ( imagesy( $imageObj ) + abs( max( $box[5], $box[7] ) ) ) / 2 );

		imagettftext( $imageObj, $fontSize, 0, $x, $y, $txtColor, $fontFile, $this->letter );

		ob_start();
		imagejpeg( $imageObj, null, 100 );
		$image = "data:image/jpeg;base64," . base64_encode( ob_get_contents() );
		ob_end_clean();

		imagedestroy( $imageObj );

		return $image;
	}
}