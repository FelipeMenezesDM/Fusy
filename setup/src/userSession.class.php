<?php
/**
 * Controlador de sessões do usuário.
 *
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class userSession {
	/**
	 * Erro de autenticação do usuário.
	 * @access private
	 * @var    boolean
	 */
	private $error = true;

	/**
	 * Método construtor.
	 * @param string  $username Nome de usuário válido.
	 * @param string  $password Senha de usuário válida.
	 * @param boolean $persist  Definir se a sessão do usuário deve ser persistida.
	 */
	public function __construct( $username, $password, $persist = false ) {
		$user = new User();

		# Consultar usuário.
		if( !$user->getByAuthFields( $username ) || is_null( $user->getUserID() ) ) {
			$this->error = gettext( "The informed user does not have an active registration." );
		}
		# Autenticar usuário com a senha informada.
		elseif( !defaultPasswordHashValidate( $password, $user->getPassword() ) ) {
			$this->error = gettext( "User and password combination is incorrect." );
		}else{
			# Informações do cookie.
			$cookieName = md5( $user->getUsername() );
			$cookieHash = md5( uniqid( rand(), true ) );

			# Verificar se já existe sessão ativa para o usuário informado.
			if( isset( $_COOKIE[ ( "fusy-session-" . $cookieName ) ] ) ) {
				$this->error = gettext( "There is already an active session on this device for the informed user." );
			}else{
				$this->error = false;
				$expire = 0;

				# Definir tempo de expiração da sessão do usuário.
				if( $persist )
					$expire = time() + 60 * 60 * 24 * 365 * 100; # Cookie expira em 100 anos.

				# Salvar informações de sessão do usuário.
				$session = new Session();
				$session->setSessionID( $cookieHash );
				$session->setSessionUserID( $user->getUserId() );
				$session->setSessionPersist( (int) $persist );
				$session->setSessionIP( getUserIP() );
				$session->post();

				# Criar cookie para persistência da sessão do usuário.
				self::setCookie( "fusy-session-" . $cookieName, $cookieHash, $expire );
			}
		}
	}

	/**
	 * Obter erro na autenticação do usuário, caso exista.
	 * @return boolean
	 */
	public function getError() {
		return $this->error;
	}

	/**
	 * Criação de cookies padronizada.
	 * @param string $id     ID do cookie.
	 * @param string $value  Valor do cookie.
	 * @param string $expire Expiração do cookie.
	 */
	public static function setCookie( $id, $value, $expire = null ) {
		if( is_null( $expire ) )
			$expire = time() + 60 * 60 * 24 * 365 * 100;

		setcookie( $id, $value, $expire, "/" );
	}

	/**
	 * Remoção de cookie padronizada.
	 * @param string $id ID do cookie.
	 */
	public static function removeCookie( $id ) {
		setcookie( $id, "", time() - 300, "/" );
	}

	/**
	 * Obter todas as sessões ativas do usuário.
	 * @return array
	 */
	public static function getSessions() {
		global $currentUser;
		$sessions = array();

		# Obter sessões locais.
		foreach( $_COOKIE as $name => $value ) {
			if( strpos( $name, "fusy-session-" ) === 0 )
				$sessions[ ( $name ) ] = $value;
		}

		# Tratamento para quando não houver sessões ativas.
		if( empty( $sessions ) )
			return array();

		# Obter informações de sessões ativas.
		$session = new Session();
		$session->get( array(
			array(
				"key"		=> "sessionStatus",
				"value"		=> 1
			),
			array(
				"key"		=> "sessionId",
				"value"		=> array_values( $sessions ),
				"compare"	=> "IN"
			)
		), array(), array( "sessionId", "sessionLastAccess", "userId", "nickname", "username", "fullname" ) );

		# Verificar erro no login.
		if( $session->getError() )
			easyNotification::setMessage( "loginCallback", easyNotification::ERROR, gettext( "Internal error: unable to complete login." ) );

		# Listar sessões ativas.
		do {
			$key = array_search( $session->getSessionID(), $sessions );
			$sessions[ ( $key ) ] = $session->getFields();
		}while( $session->next() );

		$i = 0;

		# Remover sessões inativas e definir usuário atual, caso exista.
		foreach( $sessions as $name => $session ) {
			$i++;

			if( !is_array( $session ) ) {
				unset( $sessions[ ( $name ) ] );
				self::removeCookie( $name );
				continue;
			}

			# Definir usuário atual.
			if( $i === count( $sessions ) ) {
				$currentUser = new User();
				$currentUser->getById( $session[ "userid" ] );
				$currentUser->setSessionId( $session[ "sessionid" ] );
				$userMeta = getUserMeta( array( "language", "timezone" ) );

				# Definir idioma e fuso horário do sistema.
				setSystemLanguage( ( isset( $userMeta[ "language" ] ) ? $userMeta[ "language" ] : getSetting( "language" ) ) );
				setSystemTimezone( ( isset( $userMeta[ "timezone" ] ) ? $userMeta[ "timezone" ] : getSetting( "timezone" ) ) );
			}
		}

		return $sessions;
	}
}