<?php
/**
 * Controlador de ativos do Framework.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class assetsController {
	/**
	 * Lista de estilos.
	 * @access private
	 * @var    array
	 */
	private static $styles = array();

	/**
	 * Lista de scripts
	 * @access private
	 * @var    array
	 */
	private static $scripts = array();

	/**
	 * Definir nova folha de estilos.
	 * @param string $id           ID do grupo.
	 * @param string $src          Link para o ativo.
	 * @param array  $dependencies Lista de dependências do ativo.
	 * @param string $version      Versão do arquivo.
	 */
	public static function setStyle( $id, $src, $dependencies = array(), $version = "" ) {
		$src = trim( $src );
		$key = strtolower( $src );

		if( !isset( self::$styles[ ( $id ) ] ) )
			self::$styles[ ( $id ) ] = array();

		if( !in_array( $key, array_keys( self::$styles[ ( $id ) ] ) ) )
			self::$styles[ ( $id ) ][ ( $key ) ] = array( "src" => ( !empty( $version ) ? addQueryArg( "v", $version, $src ) : $src ), "dep" => $dependencies, "loaded" => false );
	}

	/**
	 * Definir novo script.
	 * @param string $id           ID do grupo.
	 * @param string $src          Link para o ativo.
	 * @param array  $dependencies Lista de dependências do ativo.
	 * @param string $version      Versão do arquivo.
	 */
	public static function setScript( $id, $src, $dependencies = array(), $version = "" ) {
		$src = trim( $src );
		$key = strtolower( $src );

		if( !isset( self::$scripts[ ( $id ) ] ) )
			self::$scripts[ ( $id ) ] = array();

		if( !in_array( $key, array_keys( self::$scripts[ ( $id ) ] ) ) )
			self::$scripts[ ( $id ) ][ ( $key ) ] = array( "src" => ( !empty( $version ) ? addQueryArg( "v", $version, $src ) : $src ), "dep" => $dependencies, "loaded" => false );
	}

	/**
	 * Imprimir todas as folhas de estilo.
	 * @param string $id ID do grupo, caso seja necessário imprimir apenas um grupo de estilos definido.
	 */
	public static function getStyles( $id = "" ) {
		if( !empty( $id ) && !isset( self::$styles[ ( $id ) ] ) )
			return;
		elseif( !empty( $id ) )
			$styles = array( & self::$styles[ ( $id ) ] );
		else
			$styles = & self::$styles;

		foreach( $styles as & $assets ) {
			foreach( $assets as & $style ) {
				if( $style[ "loaded" ] )
					continue;

				$style[ "loaded" ] = true;

				foreach( (array) $style[ "dep" ] as $dep ) {
					if( !empty( $dep ) )
						getStyles( $dep );
				}

				echo '<link rel="stylesheet" type="text/css" href="' . $style[ "src" ] . '" />';
			}
		}
	}

	/**
	 * Imprimir todas os scripts.
	 * @param string $id ID do grupo, caso seja necessário imprimir apenas um grupo de scripts definido.
	 */
	public static function getScripts( $id = "" ) {
		if( !empty( $id ) && !isset( self::$scripts[ ( $id ) ] ) )
			return;
		elseif( !empty( $id ) )
			$scripts = array( & self::$scripts[ ( $id ) ] );
		else
			$scripts = & self::$scripts;

		foreach( $scripts as & $assets ) {
			foreach( $assets as & $script ) {
				if( $script[ "loaded" ] )
					continue;

				$script[ "loaded" ] = true;

				foreach( (array) $script[ "dep" ] as $dep ) {
					if( !empty( $dep ) )
						getScripts( $dep );
				}

				echo '<script type="text/javascript" src="' . $script[ "src" ] . '"></script>';
			}
		}
	}
}