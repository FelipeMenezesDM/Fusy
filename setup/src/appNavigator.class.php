<?php
/**
 * Controlador para os itens do menu.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class appNavigator {
	/**
	 * Lista de menus cadastrados.
	 * @access private
	 * @var    array
	 */
	private static $menus = array();

	/**
	 * Lista de ponteiros auxiliares para itens do menu.
	 * @access private
	 * @var    array
	 */
	private $pointers = array();

	/**
	 * Lista de itens do menu.
	 * @access private
	 * @var    array
	 */
	private $items = array();

	/**
	 * Item selecionado do menu.
	 * @access private
	 * @var    array
	 */
	private $currentItem = null;

	/**
	 * ID do menu.
	 * @access private
	 * @var    string
	 */
	private $id;

	/**
	 * Título do menu.
	 * @access private
	 * @var    string
	 */
	private $pageTitle;

	/**
	 * Profundidade do menu.
	 * @access private
	 * @var    integer
	 */
	private $depth;

	/**
	 * Parâmetros de navegação de acordo com a profundidade do menu.
	 * @access private
	 * @var    array
	 */
	private $nav = array(
		"page",
		"subpage",
		"section",
		"tab"
	);

	/**
	 * Identificar se o item padrão do menu foi definido.
	 * @access private
	 * @var    boolean
	 */
	private $hasDefaultItem = false;

	/**
	 * Identificar existência de menu principal.
	 * @access private
	 * @var    boolean
	 */
	private static $issetPrimary = false;

	/**
	 * Identificar se o menu foi definido como menu principal.
	 * @access private
	 * @var    boolean
	 */
	private $isPrimary = false;

	/**
	 * Método construtor.
	 * @param string  $id        ID único do menu.
	 * @param string  $pageTitle Incremento para título de página.
	 * @param string  $handler   Difinir parâmetro de manipulação da navegação do menu.
	 * @param boolean $primary   Definir se é o menu principal.
	 * @param integer $depth     Profundidade máxima do menu, onde o limite máximo será 4.
	 */
	public function __construct( $id, $pageTitle, $handler, $primary = false, $depth = 4 ) {
		$this->id = $this->slugify( $id );
		$this->pageTitle = $pageTitle;
		$this->depth = max( 1, min( $depth, 4 ) );
		$this->nav[0] = $handler;
		self::$menus[ ( $this->id ) ] = $this;

		if( !self::$issetPrimary && $primary )
			$this->isPrimary = self::$issetPrimary = $primary;
	}

	/**
	 * Tratamento para ID de menus e itens de menu.
	 * @access private
	 * @param  string  $id ID para tratamento.
	 * @return string
	 */
	private static function slugify( $id ) {
		return easyForm::slugify( $id );
	}

	/**
	 * Adicionar item ao menu.
	 * @param array $setts Definições do item do menu.
	 */
	public function addItem( $setts = array() ) {
		$setts = array_merge( array(
			"page_title"	=> "",
			"title"			=> "",
			"id"			=> "",
			"slug"			=> "",
			"url"			=> "",
			"role"			=> array(),
			"icon"			=> "",
			"order"			=> 10,
			"params"		=> array(),
			"parent"		=> "",
			"page"			=> "",
			"hide"			=> false,
			"current"		=> false,
			"default"		=> false
		), arrayKeyHandler( $setts ) );
		$setts[ "children" ] = array();
		$setts[ "level" ] = 1;
		$setts[ "order" ] = max( 1, (int) $setts[ "order" ] );
		$setts[ "id" ] = $this->slugify( $setts[ "id" ] );
		$setts[ "parent" ] = trim( $setts[ "parent" ] );
		$setts[ "page" ] = trim( $setts[ "page" ] );
		$setts[ "parents" ] = array();
		$setts[ "slug" ] = ( empty( trim( $setts[ "slug" ] ) ) ? $setts[ "id" ] : trim( $setts[ "slug" ] ) );
		$setts[ "page_title" ] = ( !empty( $setts[ "page_title" ] ) ? $setts[ "page_title" ] : $setts[ "title" ] ) . ( !empty( $this->pageTitle ) ? " | " . $this->pageTitle : "" );

		# Definição do item padrão do menu. Apenas um item padrão por menu.
		if( !$this->hasDefaultItem && $setts[ "default" ] && empty( $setts[ "parent" ] ) ) {
			$this->hasDefaultItem = $setts[ "id" ];
			$setts[ "hide" ] = false; # O item padrão deve sempre estar visível.
		}else{
			$setts[ "default" ] = false;
		}

		$iKey = strtolower( $setts[ "id" ] );
		$pKey = strtolower( $setts[ "parent" ] );

		# Não pode haver itens com chaves duplicadas.
		if( empty( $iKey ) || isset( $this->pointers[ ( $iKey ) ] ) )
			return;

		$this->pointers[ ( $iKey ) ] = $setts;
		$item = & $this->pointers[ ( $iKey ) ];
		$isFirst = false;

		# Adicionar subitens.
		if( isset( $this->pointers[ ( $pKey ) ] ) ) {
			$parent = & $this->pointers[ ( $pKey ) ];
			$level = ( $level = $parent[ "level" ] + 1 );
			$item[ "level" ] = min( $this->depth, $level );

			# Caso a profundidade seja maior que a permitida, o item vai para o último nível.
			if( $level > $this->depth ) {
				$pKey = $parent[ "parent" ];
				$item[ "parent" ] = $pKey;
			}
			
			$item[ "parents" ] = $parent[ "parents" ];
			$item[ "parents" ][] = $item[ "parent" ];
			$parent[ "children" ][] = & $item;

			# Ordenar registros.
			$this->order( $parent[ "children" ] );
			$isDefault = ( $item[ "id" ] === $parent[ "children" ][0][ "id" ] );
		}else{
			$item[ "parent" ] = "";
			$this->items[] = & $item;

			# Ordenar registros.
			$this->order( $this->items );
			$isDefault = ( $this->hasDefaultItem && $item[ "default" ] );
		}

		# Monstagem de URL do item.
		if( empty( $item[ "url" ] ) ) {
			$parents = $item[ "parents" ];
			$parents[] = $item[ "slug" ];
			$navigation = array_combine( array_slice( $this->nav, 0, count( $parents ) ), $parents );
			$item[ "url" ] = addQueryArgs( $item[ "params" ], addQueryArgs( $navigation, getAppURL() ) );
		}

		# Definir se o item está selecionado atualmente.
		$reqParam = trim( strtolower( getRequestParam( $this->nav[ ( $item[ "level" ] - 1 ) ] ) ) );
		$pReqParam = ( $item[ "level" ] === 1 && empty( trim( getRequestParam( $this->nav[0] ) ) ) || ( $item[ "level" ] > 1 && $parent[ "current" ] ) );
		$item[ "current" ] = ( $pReqParam && empty( $reqParam ) && $isDefault && $this->isPrimary ) || $reqParam === strtolower( $item[ "id" ] );

		$this->setCurrentItem();
	}

	/**
	 * Definir valor do atributo "current" em cascata.
	 * @access private
	 * @param  array   $item  Ponteiro para item que deverá ser alterado.
	 * @param  boolean $value Valor para definição do atributo.
	 */
	private function setCurrentAttr( & $item, $value = false ) {
		$item[ "current" ] = $value;

		if( !empty( $item[ "parent" ] ) )
			$this->setCurrentAttr( $this->pointers[ strtolower( $item[ "parent" ] ) ], $value );
	}

	/**
	 * Definir item selecionado do menu principal.
	 * @access private
	 */
	private function setCurrentItem() {
		$items = array_values( $this->pointers );
		$this->order( $items, "level" );
		$this->currentItem = null;

		$current = null;

		foreach( array_reverse( $items ) as $item ) {
			if( $item[ "current" ] && empty( $item[ "children" ] ) ) {
				if( is_null( $current ) )
					$current = & $this->pointers[ strtolower( $item[ "id" ] ) ];
			}

			$this->pointers[ strtolower( $item[ "id" ] ) ][ "current" ] = false;
		}

		if( !is_null( $current ) ) {
			$parentIsSelected = true;
		
			foreach( $current[ "parents" ] as $i => $parent ) {
				$param = trim( strtolower( getRequestParam( $this->nav[ ( $i ) ] ) ) );

				if( $parentIsSelected && !empty( $param ) && $param !== strtolower( $parent ) )
					$parentIsSelected = false;
			}

			if( $parentIsSelected ) {
				$this->setCurrentAttr( $current, true );
				$this->currentItem = $current;
			}
		}
	}

	/**
	 * Ordenar registros.
	 * @access private
	 * @param  array   $items Lista para ordenação.
	 * @param  string  $attr  Atributo base para ordenação.
	 */
	private function order( & $items, $attr = "order" ) {
		self::uasort( $items, function( $a, $b, $attr ) {
			if( $a[ ( $attr ) ] == $b[ ( $attr ) ] )
				return 0;
			
			return ( $a[ ( $attr ) ] < $b[ ( $attr ) ] ) ? -1 : 1;
		}, $attr );
	}

	/**
	 * Obter todos os itens do menu.
	 * @return array
	 */
	public function getItems() {
		return $this->items;
	}

	/**
	 * Obter um item do menu.
	 * @param  string $id ID único do item.
	 * @return array
	 */
	public function getItem( $id ) {
		$id = strtolower( trim( $this->slugify( $id ) ) );

		if( isset( $this->pointers[ ( $id ) ] ) )
			return $this->pointers[ ( $id ) ];

		return false;
	}

	/**
	 * Obter o objeto de um menu.
	 * @param  string $id ID único do menu.
	 * @return object
	 */
	public static function getMenuObj( $id ) {
		$id = self::slugify( $id );

		if( isset( self::$menus[ ( $id ) ] ) )
			return self::$menus[ ( $id ) ];
	}

	/**
	 * Gerar estrutura do menu recursivamente.
	 * @access private
	 * @param  array   $items   Parâmetro auxiliar para recursividade.
	 * @param  array   $parents ID de itens anteriores.
	 * @return string
	 */
	private function generate( $items, $parents = array() ) {
		$mnItems = "";

		foreach( $items as $item ) {
			$itemParents = $parents;
			$itemParents[] = $item[ "id" ];
			$hasChildren = !empty( $item[ "children" ] );

			# Montagem recursiva de subitens.
			$children = ( $hasChildren ? $this->generate( $item[ "children" ], $itemParents ) : "" );
			$miniTabID = $this->id . "-" . ( count( $item[ "parents" ]  ) >= 2 ? $item[ "parents" ][1] : $item[ "slug" ] );
			$itemID = $this->id . "-" . $item[ "slug" ];

			# Verificar permissões de acesso.
			if( !getUserRole( $item[ "role" ] ) || $item[ "hide" ] )
				continue;

			# Montagem de link do menu.
			$mnItems .= '<li class="' . $miniTabID . ' item-' . $itemID . ' item-level-' . $item[ "level" ] . ( $hasChildren ? " has-submenu" : "" ) . ( $item[ "current" ] ? " selected hightlight" : "" ) . ( $item[ "default" ] ? " is-default" : "" ) . '">' .
							'<a href="' . $item[ "url" ] . '" data-item-id="' . $itemID . '" data-tab-id="' . $miniTabID . '">' .
								( $item[ "level" ] === 1 ? '<span class="icon"><i class="fas fa-' . $item[ "icon" ] . '"></i></span>' : "" ) .
								'<span class="title">' . $item[ "title" ] . '</span>' .
							'</a>' .
							$children .
						'</li>';
		}

		$menu = '<ul class="' . ( ( count( $parents ) + 1 ) === 1 ? "menu menu-" . $this->id : "submenu submenu-" . end( $parents ) ) . '">' . $mnItems . '</ul>';

		return $menu;
	}

	/**
	 * Obter estrutura do menu.
	 * @return string
	 */
	public function getMenu() {
		return $this->generate( $this->items );
	}

	/**
	 * Obter menu de links simples.
	 * @param  string $id ID do item para obter links.
	 * @return string
	 */
	public function getAlignedMenu( $id ) {
		$item = $this->getItem( $id );

		if( $item[ "level" ] == 1 )
			$children = $this->getItems();
		else
			$children = $this->getItem( $item[ "parent" ] )[ "children" ];

		if( empty( $children ) )
			return "";

		$menu = '<ul class="default-link-menu">';
		asort( $children );

		foreach( $children as $child ) {
			$menuID = $this->id . "-" . ( count( $child[ "parents" ] ) >= 2 ? $child[ "parents" ][1] : $child[ "slug" ] );
			$itemID = $this->id . "-" . $child[ "slug" ];

			if( !$child[ "hide" ] )
				$menu .= '<li' . ( $child[ "current" ] ? ' class="current"' : "" ) . '>' .
							'<a href="' . $child[ "url" ] . '" data-item-id="' . $itemID . '" data-tab-id="' . $menuID . '">' . $child[ "title" ] . '</a>' .
						'</li>';
		}

		$menu .= '</ul>';

		return $menu;

		return "";
	}

	/**
	 * Obter o item selecionado do menu.
	 * @return array
	 */
	public function getCurrentItem() {
		return $this->currentItem;
	}

	/**
	 * Obter o ID do menu.
	 * @return string
	 */
	public function getID() {
		return $this->id;
	}

	/**
	 * Obter URL de item do menu.
	 * @param  string $itemId ID único do item para obter URL.
	 * @return string
	 */
	public function getURL( $itemId ) {
		$item = $this->getItem( $itemId );

		if( !$item )
			return "";

		return $item[ "url" ];
	}

	/**
	 * Função para ordenação por valor estável em versões do PHP anteiores ao 8.
	 * @param  array  &$array   Lista para ordenação.
	 * @param  object $function Funcão para aplicar comparação de ordenação.
	 * @param  string $attr     Atributo base para ordenação.
	 */
	public static function uasort( & $array, $function, $attr = "order" ) {
		if( count( $array ) < 2 )
			return;

		$halfway = count( $array ) / 2;
		$array1 = array_slice( $array, 0, $halfway );
		$array2 = array_slice( $array, $halfway );

		self::uasort( $array1, $function, $attr );
		self::uasort( $array2, $function, $attr );

		if( call_user_func( $function, end( $array1 ), $array2[0], $attr ) < 1 ) {
			$array = array_merge( $array1, $array2 );
			return;
		}

		$array = array();
		$ptr1 = $ptr2 = 0;

		while( $ptr1 < count( $array1 ) && $ptr2 < count( $array2 ) ) {
			if( call_user_func( $function, $array1[ ( $ptr1 ) ], $array2[ ( $ptr2 ) ], $attr ) < 1 )
				$array[] = & $array1[ ( $ptr1++ ) ];
			else
				$array[] = & $array2[ ( $ptr2++ ) ];
		}

		while( $ptr1 < count( $array1 ) )
			$array[] = $array1[ ( $ptr1++ ) ];

		while( $ptr2 < count( $array2 ) )
			$array[] = $array2[ ( $ptr2++ ) ];

		return;
	}

	/**
	 * Obter o ID do item padrão do menu.
	 * @return string
	 */
	public function getDefaultItemID() {
		return $this->hasDefaultItem;
	}
}