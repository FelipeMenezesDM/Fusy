<?php
/**
 * Estruturar links em formato de cartões.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class linkCard {
	/**
	 * Lista de cartões.
	 * @access private
	 * @var    array
	 */
	private $cards = array();

	/**
	 * Parâmetro de manipulação dos cartões. 
	 * @access private
	 * @var    string
	 */
	private $handler = null;

	/**
	 * Método construtor.
	 * @param string $handler Definir parâmetro de manipulação dos cartões.
	 */
	public function __construct( $handler ) {
		$this->handler = $handler;
	}

	/**
	 * Adicionar novo cartão.
	 * @param array $setts Definições do cartão.
	 */
	public function addCard( $setts ) {
		$setts = array_merge( array(
			"title"		=> "",
			"icon"		=> "",
			"role"		=> array(),
			"handler"	=> "",
			"class"		=> array(),
			"form"		=> ""
		), arrayKeyHandler( $setts ) );

		if( empty( $setts[ "title" ] ) || empty( $setts[ "handler" ] ) || empty( $setts[ "form" ] ) )
			return;

		if( !is_array( $setts[ "class" ] ) )
			$setts[ "class" ] = (array) $setts[ "class" ];

		$cardHandler = strtolower( trim( $setts[ "handler" ] ) );
		$this->cards[ ( $cardHandler ) ] = $setts;
	}

	/**
	 * Gerar estrutura do cartão.
	 * @access private
	 * @return string
	 */
	private function generate() {
		$cards = '<div class="link-cards-wrapper">';

		foreach( $this->cards as $card ) {
			# Verificar se o usuário possui privilégios para acessar o card.
			if( getUserRole( $card[ "role" ] ) )
				$cards .= '<div class="link-card-item">' .
							'<a href="' . $this->getURL( $card[ "handler" ] ) . '" class="link-card' . ( !empty( $card[ "class" ] ) ? " " . implode( " ", $card[ "class" ] ) : "" ) . '" title="' . $card[ "title" ] . '">' .
								'<i class="fas fa-' . $card[ "icon" ] . '"></i>' .
								'<span class="title">' . $card[ "title" ] . '<span>' .
							'</a>' .
						  '</div>';
		}

		$cards .= '<div class="clearfix"></div></div>';

		return $cards;
	}

	/**
	 * Obter URL do cartão a partir da página atual.
	 * @access private
	 * @param  array   $handler Manipulador do cartão.
	 * @return string
	 */
	private function getURL( $handler = null ) {
		global $currentSetupMenuItem;

		if( is_null( $this->handler ) || is_null( $handler ) )
			return $currentSetupMenuItem[ "url" ];

		return addQueryArg( $this->handler, $handler, $currentSetupMenuItem[ "url" ] );
	}

	/**
	 * Obter estrutura dos cartões.
	 * @param  boolean $echo Definir se deve ser imprimido.
	 * @return string
	 */
	public function getCards() {
		$handler = strtolower( trim( getRequestParam( $this->handler ) ) );

		if( isset( $this->cards[ ( $handler ) ] ) ) {
			global $currentSetupMenuItem;

			# Texto do botão "voltar".
			if( !is_null( $currentSetupMenuItem ) )
				$text = $currentSetupMenuItem[ "title" ];
			else
				$text = gettext( "Back" );

			$menuCards = '<div class="link-card-page-menu ef-table-height-line">' .
							'<a href="' . $this->getURL() . '" class="page-back-link"><i class="fas fa-arrow-left"></i> ' . $text . '</a>';

			# Menu de navegação entre cartões.
			if( count( $this->cards ) > 1 ) {
				$menuCardsItems = "";
				$cards = $this->cards;
				asort( $cards );

				foreach( $cards as $card )
					$menuCardsItems .= '<li' . ( $card[ "handler" ] === $handler ? ' class="current"' : "" ) . '><a href="' . $this->getURL( $card[ "handler" ] ) . '">' . $card[ "title" ] . '</a></li>';

				$menuCards .= '<div class="drop-down-container drop-down-autoclose all-cards-nav">' .
								'<a href="#" class="drop-down-handler">' . gettext( "Go to" ) . '<i class="fas fa-chevron-down"></i></a>' .
								'<div class="drop-down-menu floating-box">' .
									'<ul class="default-link-menu">' . $menuCardsItems . '</ul>' .
								'</div>' .
							  '</div>';
			}

			$menuCards .= '<div class="clearfix"></div></div>';

			echo $menuCards;

			getForm( $this->cards[ ( $handler ) ][ "form" ] );
			return;
		}

		echo $this->generate();
	}
}