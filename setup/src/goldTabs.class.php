<?php
/**
 * Estruturar abas para formulários.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class goldTabs {
	/**
	 * Lista de abas.
	 * @access private
	 * @var    array
	 */
	private $tabs = array();

	/**
	 * Parâmetro de manipulação das abas. 
	 * @access private
	 * @var    string
	 */
	private $handler = null;

	/**
	 * Definir se existe aba padrão.
	 * @access private
	 * @var    boolean
	 */
	private $hasDefault = false;

	/**
	 * Método construtor.
	 * @param string $handler Definir parâmetro de manipulação das abas.
	 */
	public function __construct( $handler ) {
		$this->handler = $handler;
	}

	/**
	 * Adicionar nova aba.
	 * @param array $setts Definições da aba.
	 */
	public function addTab( $setts ) {
		$setts = array_merge( array(
			"title"		=> "",
			"legend"	=> "",
			"icon"		=> "",
			"handler"	=> "",
			"default"	=> false,
			"role"		=> array(),
			"form"		=> ""
		), arrayKeyHandler( $setts ) );

		if( empty( $setts[ "title" ] ) || empty( $setts[ "handler" ] ) || empty( $setts[ "form" ] ) )
			return;

		if( !$this->hasDefault && $setts[ "default" ] )
			$this->hasDefault = true;
		elseif( $this->hasDefault )
			$setts[ "default" ] = false;

		$tabHandler = strtolower( trim( $setts[ "handler" ] ) );
		$this->tabs[ ( $tabHandler ) ] = $setts;
	}

	/**
	 * Gerar estrutura do cartão.
	 * @access private
	 * @return string
	 */
	private function generate() {
		$tabs = "";
		$form = "";
		$handler = strtolower( trim( getRequestParam( $this->handler ) ) );

		# Listar abas.
		foreach( $this->tabs as $tabHandler => $tab ) {
			$isSelected = false;

			# Verificar se o usuário possui privilégios para acessar a aba.
			if( getUserRole( $tab[ "role" ] ) ) {
				if( ( ( empty( $handler ) || !isset( $this->tabs[ ( $handler ) ] ) ) && $this->hasDefault && $tab[ "default" ] ) || $handler === $tabHandler ) {
					$isSelected = true;
					$form = $tab[ "form" ];
				}

				$tabs .= '<a href="' . $this->getURL( $tab[ "handler" ] ) . '" class="gold-tab-item' . ( $isSelected ? " selected" : "" ) . '">' .
							'<h4>' . ( !empty( $tab[ "icon" ] ) ? '<i class="fas fa-' . $tab[ "icon" ] . '"></i>' : "" ) . $tab[ "title" ] . '</h4>' .
							( !empty( $tab[ "legend" ] ) ? '<h5>' . $tab[ "legend" ] . '</h5>' : "" ) .
						 '</a>';
			}
		}

?>
		<div class="gold-tabs-wrapper">
			<div class="gold-tabs-items"><?php echo $tabs; ?><div class="clearfix"></div></div>
			<div class="gold-tabs-form"><?php getForm( $form ); ?></div>
		</div>
<?php
	}

	/**
	 * Obter URL da aba a partir da página atual.
	 * @access private
	 * @param  string  $handler Manipulador da aba.
	 * @return string
	 */
	private function getURL( $handler = null ) {
		global $currentSetupMenuItem;

		if( is_null( $this->handler ) || is_null( $handler ) )
			return $currentSetupMenuItem[ "url" ];

		return addQueryArg( $this->handler, $handler, $currentSetupMenuItem[ "url" ] );
	}

	/**
	 * Obter estrutura das abas.
	 * @param  boolean $echo Definir se deve ser imprimido.
	 * @return string
	 */
	public function getTabs() {
		$this->generate();
	}
}