<?php
/**
 * Classe para criação de lista de imagens selecionável.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   EasyForm
 * @version   1.0.0.0
 */

class imageSelectList {
	/**
	 * Definições da lista de imagens.
	 * @access private
	 * @var    array
	 */
	private $setts = array(
		"name"		=> "",
		"size"		=> 100,
		"form"		=> true,
		"multiple"	=> false,
		"current"	=> null
	);

	/**
	 * Lista de imagens adicionadas.
	 * @access private
	 * @var    array
	 */
	private $images = array();

	/**
	 * Método construtor.
	 * @param array $setts Definições da lista de imagens.
	 */
	public function __construct( $setts ) {
		$this->setts = array_merge( $this->setts, arrayKeyHandler( $setts ) );
	}

	/**
	 * Adicionar nova imagem à lista.
	 * @param array $setts Definições da imagem.
	 */
	public function addImage( $setts ) {
		$setts = array_merge( array(
			"id"		=> "",
			"attach"	=> null,
			"position"	=> "",
			"title"		=> null
		), arrayKeyHandler( $setts ) );

		$this->images[ ( $setts[ "id" ] ) ] = $setts;
	}

	/**
	 * Gerar estrutura da lista de imagens.
	 * @access private
	 * @return string
	 */
	private function generate() {
		if( count( $this->images ) === 0 )
			return "";

		$content = '<ul class="image-select-list" data-current="' . $this->setts[ "current" ] . '">';

		foreach( $this->images as $image ) {
			if( !is_array( $image[ "position" ] ) ) {
				try {
					$position = json_decode( $image[ "position" ], true );
				}catch( Exception $e ) {
					$position = array();
				}

				$image[ "position" ] = $position;
			}

			$type = ( $this->setts[ "multiple" ] ? "checkbox" : "radio" );
			$isCurrent = (  in_array( $image[ "id" ], ( array ) $this->setts[ "current" ] ) );
			$crop = new easyCrop( $image[ "attach" ], array_merge( $image[ "position" ], array( "background" => "#FFFFFF", "base64" => true ) ) );
			$content .= '<li>' .
							'<label>' .
								'<input type="' . $type . '" name="' . $this->setts[ "name" ] . ( $this->setts[ "multiple" ] ? "[]" : "" ) . '" value="' . $image[ "id" ] . '"' . ( $isCurrent ? " checked" : "" ) . ' />' .
								'<div class="image-select-item"><div class="image-select-item-wrap"><img src="' . $crop->getBase64( $this->setts[ "size" ] ) . '" /></div>' .
							'</label>' .
						'</li>';
		}

		$content .= '</ul>';

		if( $this->setts[ "form" ] )
			$content = '<form method="GET">' . $content . '</form>';

		return $content;
	}

	/**
	 * Obter estrutura da lista de imagens.
	 * @param  boolean $echo Definir se a estrutura da lista deve ser impressa.
	 * @return string
	 */
	public function getImages( $echo = true ) {
		$images = $this->generate();

		if( $echo )
			echo $images;

		return $images;
	}
}