<?php
/**
 * Objeto da tabela de dados genéricos do usuário.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class UserMeta extends MainEntity {
	/* Override */
	const KEY = array( "metaSlug", "metaUserId" );
}