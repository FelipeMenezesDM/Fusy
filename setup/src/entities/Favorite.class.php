<?php
/**
 * Objeto da tabela de favoritos do usuário.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class Favorite extends MainEntity {
	/* Override */
	const KEY = "favoriteId";
}