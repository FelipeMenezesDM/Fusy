<?php
/**
 * Objeto da tabela de usuários.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class Person extends MainEntity {
	/* Override */
	const KEY = "personId";

	/* Override */
	const JOINS = array( array(
		"table"			=> "email",
		"type"			=> "left",
		"meta_query"	=> array(
			array( "key" => "emailPersonId", "column" => "personId" ),
			array( "key" => "emailIsPrimary", "value" => 1 )
		)
	) );

	/**
	 * Obter pessoa a partir de seu email.
	 * @param  int     $email Email da pessoa.
	 * @return boolean
	 */
	public function getByEmail( $email ) {
		$metaQuery = array( array(
			"key"	=> "emailAddress",
			"value" => $email
		) );

		$joins = array( array( "table" => "email" ) );

		return $this->get( $metaQuery, $joins );
	}

	/**
	 * Obter pessoa a partir de seu hash único.
	 * @param  string  $hash Hash único da pessoa.
	 * @return boolean
	 */
	public function getByHash( $hash ) {
		$metaQuery = array( array(
			"key"	=> "personHash",
			"value" => $hash
		) );

		return $this->get( $metaQuery );
	}
}