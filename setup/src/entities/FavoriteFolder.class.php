<?php
/**
 * Objeto da tabela de pastas de favoritos do usuário.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class FavoriteFolder extends MainEntity {
	/* Override */
	const KEY = "ffolderId";
}