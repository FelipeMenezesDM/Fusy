<?php
/**
 * Objeto da tabela de ajudantes do catálogo de objetos.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class CatalogHelper extends MainEntity {
	/* Override */
	const KEY = "helperId";
}