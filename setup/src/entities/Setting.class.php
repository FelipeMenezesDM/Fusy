<?php
/**
 * Objeto da tabela configurações do sistema.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class Setting extends MainEntity {
	/* Override */
	const KEY = "slug";
}