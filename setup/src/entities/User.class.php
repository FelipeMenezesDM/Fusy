<?php
/**
 * Objeto da tabela de usuários.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class User extends MainEntity {
	/* Override */
	const KEY = "userId";

	/* Override */
	const JOINS = array(
		array( "table" => "person" ),
		array(
			"table"			=> "email",
			"type"			=> "left",
			"meta_query"	=> array(
				array( "key" => "emailPersonId", "column" => "personId" ),
				array( "key" => "emailIsPrimary", "value" => 1 )
			)
		)
	);

	/**
	 * Obter usuário a partir de seu nome de usuário.
	 * @param  int     $username Nome de usuário do usuário.
	 * @return boolean
	 */
	public function getByUsername( $username ) {
		$metaQuery = array( array(
			"key"	=> "username",
			"value" => $username
		) );

		return $this->get( $metaQuery );
	}

	/**
	 * Obter usuário a partir de seu e-mail na tabela de pessoa.
	 * @param  string  $email E-mail e usuário.
	 * @return boolean
	 */
	public function getByEmail( $email ) {
		$metaQuery = array( array(
			"key"	=> "emailAddress",
			"value"	=> $email
		) );

		$joins = array( array( "table" => "person" ), array( "table" => "email" ) );

		return $this->get( $metaQuery, $joins );
	}

	/**
	 * Obter usuário a partir da comparação de todos os campos de autenticação.
	 * @param  string  $value Valor para comparação.
	 * @return booelan
	 */
	public function getByAuthFields( $value ) {
		$metaQuery = array( array(
			"key"	=> "username",
			"value"	=> $value
		), array(
			"key"		=> "emailAddress",
			"value"		=> $value,
			"relation"	=> "OR"
		) );

		$joins = array(
			array( "table" => "person" ),
			array( "table" => "email", "type" => "left" )
		);

		$orderBy = array( "emailIsPrimary" => "DESC" );

		return $this->get( $metaQuery, $joins, null, 1, $orderBy );
	}
}