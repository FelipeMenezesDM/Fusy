<?php
/**
 * Classe abstrata para entidades do Fusy Framework
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

abstract class MainEntity {
	/**
	 * Identificador da chave da tabela.
	 */
	const KEY = self::KEY;

	/* Override */
	const JOINS = array();

	/**
	 * Campos de dados do registro.
	 * @var array
	 */
	protected $fields = array();

	/**
	 * Guardar resultados.
	 * @access protected
	 * @var    array
	 */
	protected $results = null;

	/**
	 * Erro da requisição.
	 * @access protected
	 * @var    string
	 */
	protected $error = null;

	/**
	 * Obter registro a partir de seu ID.
	 * @param  *       $id ID do registro.
	 * @return boolean
	 */
	public function getById( $id ) {
		$key = static::KEY;

		if( is_array( $key ) )
			$key = array_values( static::KEY )[0];

		$metaQuery = array( array(
			"key"	=> $key,
			"value" => $id
		) );

		return $this->get( $metaQuery );
	}

	/**
	 * Obter informações de registro.
	 * @param  array   $metaQuery Cláusulas para obter informações do registro.
	 * @param  array   $joins     Tabelas para mesclagem.
	 * @param  array   $fields    Campos para retorno.
	 * @param  int     $perPage   Número de registros por página.
	 * @param  array   $orderBy   Ordenação da consulta.
	 * @return boolean
	 */
	public function get( $metaQuery = array(), $joins = array(), $fields = array(), $perPage = null, $orderBy = array() ) {
		$params = array(
			"meta_query"	=> $metaQuery,
			"joins"			=> ( empty( $joins ) ? static::JOINS : $joins )
		);

		if( !empty( $fields ) )
			$params[ "fields" ] = $fields;

		if( !empty( $perPage ) )
			$params[ "per_page" ] = (int) $perPage;

		if( !empty( $orderBy ) )
			$params[ "order_by" ] = $orderBy;

		# Requisição para a API.
		$data = $this->load( "get", $params );

		if( $data[ "status" ] !== "success" || count( $data[ "results" ] ) === 0 )
			return false;

		$this->results = $data[ "results" ];
		$this->next();

		return true;
	}

	/**
	 * Direcionar para o próximo item na lista de resultados.
	 * @return boolean
	 */
	public function next() {
		if( is_null( $this->results ) || count( $this->results ) === 0 )
			return false;

		$this->fields = array();
		$result = array_shift( $this->results );

		foreach( $result as $id => $value )
			$this->setProperty( $id, $value );

		return true;
	}

	/**
	 * Método de inserção na base de dados.
	 * @param  boolean $updateDuplicateKey Definir se o registro deve ser atualizado caso já exista na base.
	 * @return boolean
	 */
	public function post( $updateDuplicateKey = false ) {
		$params = array( "item" => $this->fields, "update_duplicate_key" => $updateDuplicateKey );
		$data = $this->load( "post", $params );

		if( $data[ "status" ] !== "success" )
			return false;

		return ( isset( $data[ "last_insert_id" ] ) && (bool) $data[ "last_insert_id" ] ? $data[ "last_insert_id" ] : true );
	}

	/**
	 * Método de alteração de registros da base.
	 * @return boolean
	 */
	public function put() {
		$fields = $this->fields;
		$keys = array_map( "strtolower", array_map( "trim", array_values( (array) static::KEY ) ) );

		# Verificar se o registro já foi selecionado.
		if( !array_key_exists( $keys[0], $fields ) )
			return false;

		$metaQuery = array();

		# Listar chaves.
		foreach( $keys as $key ) {
			if( !isset( $fields[ ( $key ) ] ) )
				continue;

			$metaQuery[] = array(
				"key"	=> $key,
				"value"	=> $fields[ ( $key ) ]
			);

			unset( $fields[ ( $key ) ] );
		}

		# Verificar se há restrições para remoção do registro.
		if( empty( $metaQuery ) )
			return false;

		# Verificar se há colunas para atualização.
		if( empty( $fields ) )
			return false;

		# Tratamento para campos com valor nulo.
		foreach( $fields as & $field ) {
			if( is_null( $field ) )
				$field = "NULL";
		}

		$params = array(
			"sets"			=> $fields,
			"meta_query"	=> $metaQuery
		);

		$data = $this->load( "put", $params );

		if( $data[ "status" ] !== "success" )
			return false;

		return true;
	}

	/**
	 * Método de remoção de registros da base.
	 * @return boolean
	 */
	public function delete() {
		$keys = array_map( "strtolower", array_map( "trim", array_values( (array) static::KEY ) ) );
		$metaQuery = array();

		foreach( $keys as $key ) {
			if( !isset( $this->field[ ( $key ) ] ) )
				continue;

			$metaQuery[] = array(
				"key"		=> $key,
				"value"		=> $this->fields[ ( $key ) ]
			);
		}

		# Verificar se há restrições para remoção do registro.
		if( empty( $metaQuery ) )
			return false;

		$params = array( "meta_query" => $metaQuery );
		$data = $this->load( "delete", $params );

		if( $data[ "status" ] !== "success" )
			return false;

		return true;
	}

	/**
	 * Busca de registros com filtro livre.
	 * @param  array   $setts Definições da consulta.
	 * @return boolean
	 */
	public static function _get( $setts ) {
		$data = self::_load( "get", $setts );

		if( $data[ "status" ] !== "success" || count( $data[ "results" ] ) === 0 )
			return false;

		return $data;
	}

	/**
	 * Inserção de registros com configurações livres.
	 * @param  array   $setts Definições da atualização.
	 * @return boolean
	 */
	public static function _post( $setts ) {
		$data = self::_load( "post", $setts );

		if( $data[ "status" ] !== "success" )
			return false;

		return true;
	}
	
	/**
	 * Alteração de registros com configurações livres.
	 * @param  array   $setts Definições da atualização.
	 * @return boolean
	 */
	public static function _put( $setts ) {
		$data = self::_load( "put", $setts );

		if( $data[ "status" ] !== "success" )
			return false;

		return true;
	}

	/**
	 * Remoção de registros com filtro livre.
	 * @param  array   $setts Definições da remoção.
	 * @return boolean
	 */
	public static function _delete( $setts ) {
		$data = self::_load( "delete", $setts );

		if( $data[ "status" ] !== "success" )
			return false;

		return true;
	}

	/**
	 * Criar dimicamente métodos get e set para o objeto.
	 * @param string $func  Tipo de função: get ou set.
	 * @param array $params Parâmetro de funções set.
	 */
	public function __call( $func, $params ) {
		$func = strtolower( $func );
		$action = substr( $func, 0, 3 );
		$func = substr( $func, 3 );

		if( $action === "get" && isset( $this->fields[ ( $func ) ] ) )
			return $this->fields[ ( $func ) ];
		elseif( $action === "set" && !empty( $params ) )
			$this->setProperty( $func, $params[0] );
	}

	/**
	 * Obter propriedade da entidade.
	 * @param  string $property Identificador da propriedade.
	 * @return string
	 */
	public function getProperty( $property ) {
		$property = strtolower( trim( $property ) );

		if( isset( $this->fields[ ( $property ) ] ) )
			return $this->fields[ ( $property ) ];
		
		return null;
	}

	/**
	 * Obter ID da elemento da tabela.
	 * @return string
	 */
	public function getID() {
		return $this->getProperty( static::KEY );
	}

	/**
	 * Definir valor de propriedade.
	 * @param string $property Identificador da propriedade.
	 * @param string $value    Valor da propriedade.
	 */
	public function setProperty( $property, $value ) {
		$this->fields[ strtolower( trim( $property ) ) ] = $value;
	}

	/**
	 * Requisição para execução de ação a partir da API.
	 * @access private
	 * @param  string  $action Ação para execução.
	 * @param  array   $params Parâmetros da requisição.
	 * @return array
	 */
	private static function _load( $action, $params ) {
		$url = addQueryArgs( $params, str_replace( array( "{%action%}", "{%object%}" ), array( $action, get_called_class() ), self::getRequestURL() ) );
		$data = array( "status" => "error" );

		$CURLConn = curl_init();
		curl_setopt( $CURLConn, CURLOPT_URL, $url );

		curl_setopt( $CURLConn, CURLOPT_RETURNTRANSFER, true );
		$result = curl_exec( $CURLConn );
		curl_close( $CURLConn );

		try{
			$data = json_decode( $result, true );
		}catch( Exception $e ) {
			$data[ "message" ] = "Unknown error.";
		}

		return $data;
	}

	/**
	 * Requisição para execução de ação a partir da API.
	 * @access private
	 * @param  string  $action Ação para execução.
	 * @param  array   $params Parâmetros da requisição.
	 * @return array
	 */
	private function load( $action, $params ) {
		$data = self::_load( $action, $params );

		# Salvar mensagem de erro da requisição.
		if( $data[ "status" ] !== "success" )
			$this->error = $data[ "message" ];

		return $data;
	}

	/**
	 * Obter erro de requisição.
	 * @return string
	 */
	public function getError() {
		return $this->error;
	}

	/**
	 * Obter todos os campos da entidade.
	 * @return array
	 */
	public function getFields() {
		return $this->fields;
	}

	/**
	 * Obter a URL padrão de requisições para a API.
	 * @return string
	 */
	public static function getRequestURL() {
		return getAppURL() . "index.php/api/v1.0/{%action%}/{%object%}";
	}
}