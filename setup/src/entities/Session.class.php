<?php
/**
 * Objeto da tabela sessões do usuário.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class Session extends MainEntity {
	/* Override */
	const KEY = "sessionId";

	/* Override */
	const JOINS = array(
		array( "table"	=> "user" ),
		array( "table"	=> "person" )
	);
}