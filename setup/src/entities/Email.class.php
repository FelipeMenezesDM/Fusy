<?php
/**
 * Objeto da tabela de e-mails de pessoas.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class Email extends MainEntity {
	/* Override */
	const KEY = "emailId";

	/* Override */
	const JOINS = array( array( "table" => "person" ) );

	/**
	 * Obter endereço de e-mail.
	 * @param  string  $email Endereço de e-mail.
	 * @return boolean
	 */
	public function getByEmailAddress( $email ) {
		$metaQuery = array( array(
			"key"	=> "emailAddress",
			"value" => $email
		) );

		return $this->get( $metaQuery );
	}
}