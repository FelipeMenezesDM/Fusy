<?php
/**
 * Objeto da tabela de imagem de perfil de pessoas.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2021 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

class ProfilePicture extends MainEntity {
	/* Override */
	const KEY = array( "ppictureId", "ppicturePersonId" );

	/* Override */
	const JOINS = array(
		array( "table" => "person" ),
		array( "table" => "user" )
	);
}