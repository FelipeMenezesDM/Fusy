<?php
/**
 * Arquivo de funções exclusivas para objetos.
 * 
 * @author    Felipe Menezes <contato@felipemenezes.com.br>
 * @copyright (c) 2020 Felipe Menezes
 * @package   Fusy Framework
 * @version   1.0.0.0
 */

/**
 * Validar registros duplicados na base de dados.
 * @return void
 */
function validateDuplicateRecord() {
	global $field, $form;

	# Verificar se entidade do campo foi definida.
	if( isset( $field[ "entity" ] ) && class_exists( $field[ "entity" ] ) ) {
		$entity = new $field[ "entity" ]();
		$entity->get( array( array( "key" => $field[ "name" ], "value" => $field[ "db_value" ] ) ), null, null, 1 );

		if( !is_null( $entity->getID() ) && $entity->getID() !== $form->getRegistredItemID() )
			$field[ "error" ] = gettext( "This value is already in use by another record." );
	}
}

/**
 * Validar percentual de correspondência antes da inserção de novos registros.
 * @return void
 */
function validateMatchPercentage() {
	global $field, $form;

	if( isset( $field[ "entity" ] ) && class_exists( $field[ "entity" ] ) ) {
		$percentage = isset( $field[ "percentage" ] ) ? (int) $field[ "percentage" ] : 100;
		$entity = new $field[ "entity" ]();
		$entity->get( array( array( "key" => $field[ "name" ], "value" => $field[ "db_value" ], "compare" => "MATCH_PERCENTAGE", "percentage" => $percentage ) ), null, null, 1 );

		if( !is_null( $entity->getID() ) && $entity->getID() !== $form->getRegistredItemID() )
			$field[ "error" ] = sprintf( gettext( "The label \"%s\" has more than %s%% compatibility with this new record. To continue, change the context and make sure that the similarity between the two is less than %s%%." ), $entity->getID(), $percentage, $percentage );
	}
}

/**
 * Salvar imagem de perfil do usuário.
 * @return void
 */
function savePersonPicture() {
	global $field, $form;
	$position = ( isset( $field[ "value" ][ "position" ] ) ? $field[ "value" ][ "position" ] : array() );
	$personId = $form->getRegistredItemID();

	# Salvar imagem na base de dados.
	if( isset( $field[ "value" ][ "files" ][0][ "name" ] ) && !$field[ "value" ][ "files" ][0][ "error" ] && !isset( $field[ "value" ][ "files" ][0][ "uploaded" ] ) ) {
		# Inserir imagem na tabela de imagens de perfil.
		$ppicture = new ProfilePicture();
		$ppicture->setPPicturePersonId( $personId );
		$ppicture->setPPictureAttach( $field[ "directory" ] . $field[ "value" ][ "files" ][0][ "name" ] );
		$ppicture->setPPictureName( $field[ "value" ][ "files" ][0][ "name" ] );
		$ppicture->setPPictureSetts( json_encode( $position ) );

		# Atualizar imagem de perfil na tabela de pessoas.
		if( $lastInsertedID = $ppicture->post() ) {
			$person = new Person();
			$person->setPersonPPictureId( $lastInsertedID );
			$person->setPersonId( $personId );
			$person->put();
		}

		$field[ "value" ][ "files" ][0][ "uploaded" ] = true;
	}elseif( ( !isset( $field[ "value" ][ "files" ] ) || empty( $field[ "value" ][ "files" ] ) ) && ( !isset( $field[ "value" ][ "position" ] ) || empty( $field[ "value" ][ "position" ] ) ) ) {
		# Remover imagem de perfil.
		$person = new Person();
		$person->setPersonPPictureId( null );
		$person->setPersonId( $personId );
		$person->put();

		# Remover imagem atual.
		$field[ "current" ] = null;
	}else{
		$person = new Person();
		$person->getById( $personId );

		# Atualizar imagem de perfil na tabela de pessoas.
		$ppicture = new ProfilePicture();
		$ppicture->setPPictureSetts( json_encode( $position ) );
		$ppicture->setPPictureId( $person->getPersonPPictureId() );
		$ppicture->put();
	}
}

/**
 * Salvar dado genérico do usuário.
 * @return void
 */
function saveUserMeta() {
	global $field;
	$meta = $field[ "name" ];
	$value = $field[ "value" ];
	$type = "TEXT";

	if( isset( $field[ "meta" ] ) && !empty( $field[ "meta" ] ) ) {
		$meta = $field[ "meta" ];
		$type = "LIST";

		if( !is_array( $value ) )
			$value = array( $field[ "name" ] => $value );
	}

	putUserMeta( strtolower( $meta ), $value, $type );
}

/**
 * Método padrão para salvamento de campos individuais de entidades.
 * @return void
 */
function saveProperty() {
	global $field, $form;

	# Verificar se entidade do campo foi definida.
	if( isset( $field[ "entity" ] ) && class_exists( $field[ "entity" ] ) ) {
		$entity = new $field[ "entity" ]();
		$entity->getById( $form->getRegistredItemID() );
		$entity->setProperty( strtolower( $field[ "name" ] ), $field[ "value" ] );
		$entity->put();
	}
}